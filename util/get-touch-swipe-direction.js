export default ( el, callback ) => {
	const touchSurface = el;
	let swipedir;
	let startX;
	let startY;
	let distX;
	let distY;

	const //required min distance traveled to be considered swipe
	threshold = 150;

	const // maximum distance allowed at the same time in perpendicular direction
	restraint = 100;

	const // maximum time allowed to travel that distance
	allowedTime = 300;

	let elapsedTime;
	let startTime;
	const handleSwipe = callback || ( swipedir => {} );

	touchSurface.addEventListener(
		'touchstart',
		e => {
			const touchobj = e.changedTouches[ 0 ];
			swipedir = 'none';
			dist = 0;
			startX = touchobj.pageX;
			startY = touchobj.pageY;
			startTime = new Date().getTime(); // record time when finger first makes contact with surface
			e.preventDefault();
		},
		false,
	);

	touchSurface.addEventListener(
		'touchmove',
		e => {
			e.preventDefault(); // prevent scrolling when inside DIV
		},
		false,
	);

	touchSurface.addEventListener(
		'touchend',
		e => {
			const touchobj = e.changedTouches[ 0 ];
			distX = touchobj.pageX - startX; // get horizontal dist traveled by finger while in contact with surface
			distY = touchobj.pageY - startY; // get vertical dist traveled by finger while in contact with surface
			elapsedTime = new Date().getTime() - startTime; // get time elapsed
			if ( elapsedTime <= allowedTime ) {
				// first condition for awipe met
				if (
					Math.abs( distX ) >= threshold &&
					Math.abs( distY ) <= restraint
				) {
					// 2nd condition for horizontal swipe met
					swipedir = distX < 0 ? 'left' : 'right'; // if dist traveled is negative, it indicates left swipe
				} else if (
					Math.abs( distY ) >= threshold &&
					Math.abs( distX ) <= restraint
				) {
					// 2nd condition for vertical swipe met
					swipedir = distY < 0 ? 'up' : 'down'; // if dist traveled is negative, it indicates up swipe
				}
			}
			handleSwipe( swipedir );
			e.preventDefault();
		},
		false,
	);
};
