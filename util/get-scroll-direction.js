const dirFromDeltas = ( deltaX, deltaY ) => {
	// @todo P4 Fix for touch events
	if ( typeof deltaX !== 'undefined' && typeof deltaY !== 'undefined' ) {
		let scrollDirection;
		if ( deltaY > 0 && deltaX === -0 ) {
			scrollDirection = 'down';
		} else if ( deltaY <= 0 && deltaX === -0 ) {
			scrollDirection = 'up';
		} else if ( deltaY === -0 && deltaX > 0 ) {
			scrollDirection = 'left';
		} else if ( deltaY === -0 && deltaX <= 0 ) {
			scrollDirection = 'right';
		} else if ( deltaY > 0 && deltaX > 0 ) {
			scrollDirection = 'down left';
		} else if ( deltaY > 0 && deltaX <= 0 ) {
			scrollDirection = 'down right';
		} else if ( deltaY <= 0 && deltaX > 0 ) {
			scrollDirection = 'up left';
		} else if ( deltaY <= 0 && deltaX <= 0 ) {
			scrollDirection = 'up right';
		} else {
			scrollDirection = null;
		}
		return scrollDirection;
	}
};

export default ( e, orientation = '' ) => {
	const { deltaX, deltaY } = e.nativeEvent ? e.nativeEvent : e;
	const dir = dirFromDeltas( deltaX, deltaY );
	if ( orientation === 'vertical' ) {
		if ( dir && dir.includes( 'up' ) ) {
			return 'up';
		}
		if ( dir && dir.includes( 'down' ) ) {
			return 'down';
		}
		return null;
	}
	if ( orientation === 'horizontal' ) {
		if ( dir && dir.includes( 'left' ) ) {
			return 'left';
		}
		if ( dir && dir.includes( 'right' ) ) {
			return 'right';
		}
		return null;
	}
	return dir;
};
