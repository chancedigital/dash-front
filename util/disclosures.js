export default [
	{
		id: 0,
		label: 'Member FINRA',
		url: 'http://www.finra.org/',
	},
	{
		id: 1,
		label: 'SIPC',
		url: 'http://www.sipc.com/',
	},
	{
		id: 2,
		label: 'NFA',
		url: 'https://www.nfa.futures.org/',
	},
	{
		id: 3,
		label: '606 REPORT',
		url: 'https://www.dashfinancial.com/wp-content/uploads/2018/07/606-Report_2018_Q2.pdf',
	},
	{
		id: 4,
		label: 'DISCLOSURES',
		url: 'https://www.dashfinancial.com/wp-content/uploads/2018/07/Annual-Client-Disclosures-2018.pdf',
	},
];
