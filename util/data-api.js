// @todo
class DataApi {
	constructor( rawData ) {
		this.rawData = rawData;
	}
	getPageContent() {
		const {
			date,
			slug,
			title,
			excerpt,
			featured_media,
			categories,
			acf,
		} = rawData;
		const data = {
			publishDate: date,
			slug,
			title: title.rendered,
			content: content.rendered,
			excerpt: excerpt.rendered,
			featuredMedia: featured_media,
			categories,
		};
		if ( acf ) {
			if ( acf.scroll_sections ) {
				data.scrollSections = acf.scroll_sections;
			}
			if ( acf.overview ) {
				data.overview = acf.overview;
			}
		}
		return data;
	}
	getPosts() {}
	getCategories() {}
	getTeam() {
		const { slug, title, _embedded, acf } = rawData;
		const img =
			_embedded[ 'wp:featuredmedia' ].length > 0
				? _embedded[ 'wp:featuredmedia' ][ 0 ]
				: null;
		const data = {
			slug,
			title: title.rendered,
			content: content.rendered,
			img: {
				url: img.source_url,
				alt: img.alt_text,
			},
		};
		if ( acf ) {
			if ( acf.job_title ) {
				data.jobTitle = acf.job_title;
			}
		}
		return data;
	}
}
export default DataApi;
