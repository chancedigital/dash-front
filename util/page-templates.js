module.exports = function ( slug ) {
	if ( slug === 'media' ) {
		return '/media';
	} else if ( slug === 'company' ) {
		return '/company';
	} else if ( slug === 'platform' ) {
		return '/platform';
	} else if ( slug === 'contact' ) {
		return '/contact';
	} else if ( slug === 'request-demo' ) {
		return '/request-demo';
	} else {
		return '/page';
	}
}
