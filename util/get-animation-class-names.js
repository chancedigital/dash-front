export default className => ( {
	appear: `${className}--appear`,
	appearActive: `${className}--active-appear`,
	enter: `${className}--enter`,
	enterActive: `${className}--enter-active`,
	enterDone: `${className}--done-enter`,
	exit: `${className}--leave`,
	exitActive: `${className}--leave-active`,
	exitDone: `${className}--done-leave`,
} );
