export default posts => {
	if ( posts && posts.length > 0 ) {
		return posts.map( post => {
			const {
				date,
				date_gmt,
				excerpt,
				id,
				post_categories,
				post_year,
				title,
				slug,
			} = post;
			return {
				date,
				dateGmt: date_gmt,
				excerpt: excerpt && excerpt.rendered ? excerpt.rendered : excerpt,
				id,
				categories:
					post_categories && post_categories.length > 0
						? post_categories.map( cat => ( {
								label: cat.name,
								slug: cat.slug,
								id: cat.term_id,
						  } ) )
						: null,
				year: post_year,
				title: title && title.rendered ? title.rendered : title,
				slug,
			};
		} );
	}
	return null;
};
