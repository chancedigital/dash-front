// Get top and left position of an element in relation to the window.
export default element => {
	let x = 0;
	let y = 0;

	while ( element ) {
		x += element.offsetLeft - element.scrollLeft + element.clientLeft;
		y += element.offsetTop - element.scrollTop + element.clientTop;
		element = element.offsetParent;
	}
	return { x, y };
};
