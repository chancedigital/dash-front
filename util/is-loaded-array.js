export default arr => {
	if ( typeof arr === 'undefined' ) {
		console.error( 'Variable is undefined WHOOPS!' );
		return [];
	} else if ( ! Array.isArray( arr ) ) {
		console.error( 'Variable is not an array WHOOPS!' );
		return [];
	} else if ( arr.length === 0 ) {
		console.error( 'Array is empty WHOOPS!' );
		return [];
	} else {
		return arr;
	}
};
