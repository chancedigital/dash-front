export default element => {
	// get the height of the element's inner content, regardless of its actual size
	const sectionHeight = element.scrollHeight;

	// have the element transition to the height of its inner content
	element.style.height = `${ sectionHeight }px`;

	// when the next css transition finishes (which should be the one we just triggered)
	element.addEventListener( 'transitionend', e => {
		// remove this event listener so it only gets triggered once
		element.removeEventListener( 'transitionend', arguments.callee );

		// remove "height" from the element's inline styles, so it can return to its initial value
		element.style.height = null;
	} );
};
