import FontFaceObserver from 'fontfaceobserver';

const Fonts = () => {
	const link = document.createElement( 'link' );
	link.href = 'https://fonts.googleapis.com/css?family=Muli:200,200i|Nunito+Sans:200,200i,400,400i,600,600i,700,700i|Oswald:200,300,500';
	link.rel = 'stylesheet';

	document.head.appendChild( link );

	const muli = new FontFaceObserver( 'Muli' );
	const nunito = new FontFaceObserver( 'Nunito Sans' );
	const oswald = new FontFaceObserver( 'Oswald' );

	muli.load().then( () => {
		document.documentElement.classList.add( 'muli' );
	} );
	nunito.load().then( () => {
		document.documentElement.classList.add( 'nunito' );
	} );
	oswald.load().then( () => {
		document.documentElement.classList.add( 'oswald' );
	} );
}

export default Fonts;
