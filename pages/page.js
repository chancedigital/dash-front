import React, { Component } from 'react';
import fetch from 'isomorphic-unfetch';
import Error from './_error';
import SimplePage from 'templates/simple-page';
import { Config } from 'root/config';

class Page extends Component {
	static async getInitialProps( context ) {
		let statusMessage;
		const { slug, apiRoute } = context.query;
		const res = await fetch(
			`${ Config.apiUrl }/wp-json/dash/v1/${ apiRoute }?slug=${ slug }`,
		);
		const page = await res.json();
		const statusCode = res.statusCode > 200 ? res.statusCode : null;
		switch (statusCode) {
			case 400:
				statusMessage = 'You have made a bad request.';
				break;
			case 401:
				statusMessage = 'We were unable to authenticate you to view this content.';
				break;
			case 403:
				statusMessage = 'You are not allowed to view this content.';
				break;
			case 404:
				statusMessage = 'This page could not be found. We are sad.';
				break;
			case 408:
				statusMessage = 'Your request has timed out, it seems.';
				break;
			case 500:
				statusMessage = 'Internal server error.';
				break;
			default:
				statusMessage = 'This page could not be found.';
		}

		return {
			page,
			statusMessage,
			statusCode,
		};
	}

	render() {
		const { page, statusCode, statusMessage } = this.props;
		if ( statusCode ) {
			return <Error statusCode={ statusCode } message={ statusMessage } />;
		}

		return (
			<SimplePage
				title={ page.title.rendered }
				content={ page.content.rendered }
			/>
		);
	}
}

export default Page;
