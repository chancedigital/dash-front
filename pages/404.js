import Layout from 'organisms/layout';
import React, { Component } from 'react';

class FourOhFour extends Component {

	render() {
		return (
			<Layout>
				<h1>Page not found</h1>
			</Layout>
		);
	}
}

export default FourOhFour;
