import Layout from 'organisms/layout';
import React, { Component } from 'react';
import fetch from 'isomorphic-unfetch';
import Error from 'next/error';
import { Config } from 'root/config';

class Preview extends Component {
	constructor() {
		super();
		this.state = {
			post: null,
		};
	}

	componentDidMount() {
		const { id, wpnonce } = this.props.url.query;
		fetch(
			`${
				Config.apiUrl
			}/wp-json/dash/v1/post/preview?id=${ id }&_wpnonce=${ wpnonce }`,
			{ credentials: 'include' } // required for cookie nonce auth
		)
			.then( res => res.json() )
			.then( res => {
				this.setState( {
					post: res,
				} );
			} );
	}

	render() {

		const { headerMenu } = this.props;
		const { post } = this.state;

		if (
			post &&
            post.code &&
            post.code === 'rest_cookie_invalid_nonce'
		)
			return <Error statusCode={ 404 } />;

		return (
			<Layout headerMenu={ headerMenu }>
				<h1>{ post ? post.title.rendered : '' }</h1>
				<div
					dangerouslySetInnerHTML={ {
						__html: post
							? post.content.rendered
							: '',
					} }
				/>
			</Layout>
		);
	}
}

export default Preview;
