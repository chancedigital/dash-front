import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import Layout from 'organisms/layout';
import FullScreenFrame from 'organisms/full-screen-frame';

class Error extends Component {
	static defaultProps = {
		message: 'This page could not be found.',
		statusCode: 404,
	};

	static propTypes = {
		message: PropTypes.string,
		statusCode: PropTypes.number,
	};

	render() {
		const { statusCode, message } = this.props;
		return (
			<Layout
				className="page-error"
				title={`${ statusCode }: ${ message  } | Dash`}
				bgVideo="http://d1bmh8rsgi4yr.cloudfront.net/DASH001_VideoBackgrounds_Company_2018-08-14.mp4"
			>
				<FullScreenFrame>
					<section className="page-error__content">
						<div className="page-error__code">{ statusCode }</div>
						<div className="page-error__message">{ message }</div>
					</section>
				</FullScreenFrame>
			</Layout>
		);
	}
}

export default Error;
