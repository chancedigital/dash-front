import React from 'react';
import fetch from 'isomorphic-unfetch';
import App, { Container } from 'next/app';
import { PageTransition } from 'next-page-transitions';
import Router from 'next/router';
import { debounce } from 'lodash';
import NProgress from 'nprogress';
import MobileDetect from 'mobile-detect';
import Header from 'organisms/header';
import { Config } from 'root/config';
import breakpoints from 'util/breakpoints';
import { DeviceContext } from 'context/device';

Router.events.on( 'routeChangeStart', () => NProgress.start() );
Router.events.on( 'routeChangeComplete', () => NProgress.done() );
Router.events.on( 'routeChangeError', () => NProgress.done() );

const TIMEOUT = 400;

export default class Dash extends App {
	static async getInitialProps( { Component, ctx } ) {
		let pageProps = {};
		const { req } = ctx;
		const userAgent = req ? req.headers[ 'user-agent' ] : navigator.userAgent;
		const headerMenuRes = await fetch(
			`${ Config.apiUrl }/wp-json/menus/v1/menus/header-menu`,
		);
		const headerMenu = await headerMenuRes.json();
		const mobileMenuRes = await fetch(
			`${ Config.apiUrl }/wp-json/menus/v1/menus/mobile-menu`,
		);
		const mobileMenu = await mobileMenuRes.json();
		if ( Component.getInitialProps ) {
			pageProps = await Component.getInitialProps( ctx );
		}
		return { pageProps, headerMenu, mobileMenu, userAgent };
	}

	state = {
		menuIsOpen: false,
		screenWidth: 0,
	};

	componentDidMount() {
		this.updateWindowDimensions();
		window.addEventListener( 'resize', this.updateWindowDimensions );
	}

	componentWillUnmount() {
		window.removeEventListener( 'resize', this.updateWindowDimensions );
	}

	shouldComponentUpdate( nextProps, nextState ) {
		return ! ( this.state.screenWidth === 0 && nextState.screenWidth !== 0 );
	}

	updateWindowDimensions = debounce( () => {
		if ( typeof window !== 'undefined' ) {
			const screenWidth = window.innerWidth;
			this.setState( state => {
				if (
					breakpoints.medium > screenWidth ||
					state.screenWidth < breakpoints.medium
				) {
					return { screenWidth };
				}
			} );
		}
	}, 300 );

	handleMenuChange = () => {
		this.setState( state => {
			return {
				menuIsOpen: ! state.menuIsOpen,
			};
		} );
	};

	handleMenuClose = () => {
		this.setState( state => {
			if ( state.menuIsOpen ) return { menuIsOpen: false };
		} );
	};

	render() {
		const { Component, pageProps, headerMenu, mobileMenu, userAgent } = this.props;
		const { menuIsOpen, screenWidth } = this.state;
		const mobileDetect = new MobileDetect( userAgent, breakpoints.medium - 1 );
		const device = mobileDetect.mobile()
			? 'mobile'
			: screenWidth < breakpoints.medium - 1 && screenWidth !== 0
				? 'mobile'
				: 'desktop';
		return (
			<DeviceContext.Provider value={ device }>
				<Container>
					<Header
						device={ device }
						menu={ device === 'mobile' ? mobileMenu : headerMenu }
						menuIsOpen={ menuIsOpen }
						handleMenuChange={ this.handleMenuChange }
						handleMenuClose={ this.handleMenuClose }
					/>
					{ device === 'desktop' ? (
						<PageTransition
							timeout={ TIMEOUT }
							classNames="page-transition-"
							loadingDelay={ 0 }
							loadingTimeout={ {
								enter: TIMEOUT,
								exit: 0,
							} }
							loadingClassNames="loading-indicator-"
						>
							<Component
								{ ...pageProps }
								menuIsOpen={ menuIsOpen }
								handleMenuChange={ this.handleMenuChange }
								screenWidth={ screenWidth }
							/>
						</PageTransition>
					) : (
						<Component
							{ ...pageProps }
							menuIsOpen={ menuIsOpen }
							handleMenuChange={ this.handleMenuChange }
							screenWidth={ screenWidth }
						/>
					) }
				</Container>
			</DeviceContext.Provider>
		);
	}
}
