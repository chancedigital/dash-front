const dev = process.env.NODE_ENV === 'development';
const prod = process.env.NODE_ENV === 'production';
const { PHASE_PRODUCTION_SERVER } = dev
	? {} // We're never in "production server" phase when in development mode
	: ! process.env.NOW_REGION
		? require( 'next/constants' ) // Get values from `next` package when building locally
		: require( 'next-server/constants' ); // Get values from `next-server` package when building on now v2

module.exports = ( phase, { defaultConfig } ) => {
	if ( phase === PHASE_PRODUCTION_SERVER ) {
		// Config used to run in production.
		return {};
	}
	const webpack = require( 'webpack' );
	const withSass = require( '@zeit/next-sass' );
	require( 'dotenv' ).config( {
		path: prod ? '.env.production' : '.env',
	} );
	return withSass( {
		sassLoaderOptions: {
			data: '@import "styles/global/global";',
		},
		serverRuntimeConfig: {
			mailgunApiKey: process.env.MAILGUN_API_KEY,
			mailgunDomain: process.env.MAILGUN_DOMAIN,
			contactFormTo: process.env.CONTACT_FORM_TO,
			contactFormFrom: process.env.CONTACT_FORM_FROM,
		},
		publicRuntimeConfig: {
			siteUrl: process.env.SITE_URL,
			wpUrl: process.env.WP_URL,
		},
		webpack: config => {
			const env = Object.keys( process.env ).reduce( ( acc, curr ) => {
				acc[ `process.env.${ curr }` ] = JSON.stringify( process.env[ curr ] );
				return acc;
			}, {} );
			config.plugins.push( new webpack.DefinePlugin( env ) );
			return config;
		},
	} );
}
