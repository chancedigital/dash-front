# Dash

1. This project will require a `.env` file (or `.env.production` on production) at the root directory with the following keys:

  ```dotEnv
  MAILGUN_API_KEY='YOUR API KEY HERE'
  MAILGUN_DOMAIN='YOUR VERIFIED MAILGUN DOMAIN HERE'
  CONTACT_FORM_TO='deliver.to@email.com'
  CONTACT_FORM_FROM='deliver.from@email.com'
  SITE_URL='https://dashfinancial.com'
  WP_URL='https://wp.dashfinancial.com'
  ```

> NOTE: You will also need to copy the public-facing variables `SITE_URL` and `WP_URL` into `now.json` before deploying.

2. Check your version of Node against the `.nvmrc` file, then run `yarn && yarn dev` to start locally.
3. Run `yarn deploy` to push to production.

## Migrating from Mailgun
The contact form is currently configured to submit via Mailgun. When the form is submitted, it hits an API endpoint registered in `controllers/contact.js` that imports the Mailgun function from `services/mailgun.js`. You can replace the Mailgun service with any other method you'd like to intercept the API request.
