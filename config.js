import getConfig from 'next/config';
const { publicRuntimeConfig } = getConfig();

export const Config = {
	url: publicRuntimeConfig ? publicRuntimeConfig.siteUrl : '',
	apiUrl: publicRuntimeConfig ? publicRuntimeConfig.wpUrl : process.env.WP_URL,
};
