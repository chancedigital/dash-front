export const contacts = [
	{
		name: 'Trade Desk',
		email: 'tradedesk@dashfinancial.com',
		phone: '(888) 569-3270',
	},
	{
		name: 'Concierge',
		email: 'concierge@dashfinancial.com',
		phone: '(888) 569-3274',
	},
	{
		name: 'Sales',
		email: 'sales@dashfinancial.com',
		phone: '(844) 569-3200',
	},
];
export const locations = [
	{
		id: 0,
		name: 'New York',
		address: '250 Park Ave. S.,<br />9th Floor<br />New York, NY 10003',
		phone: '(888) 569-3274',
		img: 'bg_contact_new_york',
	},
	{
		id: 1,
		name: 'Chicago',
		address: '311 South Wacker Drive,<br />Suite 1000<br />Chicago, IL 60606',
		phone: '(312) 986-2006',
		img: 'bg_contact_chicago',
	},
];
export const socialNav = [
	{
		name: 'Facebook',
		link: 'https://www.facebook.com/',
	},
	{
		name: 'Twitter',
		link: 'https://twitter.com/dashfinancial',
	},
	{
		name: 'LinkedIn',
		link: 'https://www.linkedin.com/company/dash-financial',
	},
];
