export default [
	{
		title: {
			rendered: 'The Culture',
		},
		content: {
			rendered: 'At Dash, our company values are not only the principles that link us together in the way we work, but they also keep us aligned on the same path, all working toward our collective goals.',
		},
	},
	{
		title: {
			rendered: 'Innovation',
		},
		content: {
			rendered: 'First and foremost, we’re a technology company. The system we built evolves and adjusts to our client’s needs. Dash team members always embrace change, open themselves up to new ideas, and deliver inventive thinking.',
		},
	},
	{
		title: {
			rendered: 'Transparency',
		},
		content: {
			rendered: 'Openness permeates everything we do. It is as much a part of the way we interact with each other and our clients, as it is crafted into our technology and products. We are champions of an open approach in every aspect of how we conduct business.',
		},
	},
	{
		title: {
			rendered: 'Drive',
		},
		content: {
			rendered: 'We’re laser-focused on unleashing the unlimited potential ahead of us all. We measure our success as a company, as a team, and as individuals by the technological innovation and client performance we achieve.',
		},
	},
	{
		title: {
			rendered: 'Integrity',
		},
		content: {
			rendered: 'Dash is never aloof or condescending. We are always open-minded and willing to listen, clear and un-conflicted in everything we do.',
		},
	},
	{
		title: {
			rendered: 'Craftsmanship',
		},
		content: {
			rendered: 'Our expertise is directed at constantly developing and improving solutions. As true craftsmen, we are precise, and never settle for “good enough” – only better and better, yet.',
		},
	},
	{
		title: {
			rendered: 'Focus',
		},
		content: {
			rendered: 'We use our expertise to promote a more open approach to trading. By simplifying complex issues and helping our clients define their goals, we serve as guides along the path to better performance.',
		},
	},
];
