const nodemailer = require( 'nodemailer' );
const mg = require( 'nodemailer-mailgun-transport' );
const mailgunAuth = {
	auth: {
		api_key: process.env.MAILGUN_API_KEY,
		domain: process.env.MAILGUN_DOMAIN, // https://mailgun.com/app/domains
	},
};
const nodemailerMailgun = nodemailer.createTransport( mg( mailgunAuth ) );

module.exports = async function( { body } ) {
	const { demo, firstName, company, email } = await body;
	await nodemailerMailgun.sendMail(
		{
			from: process.env.CONTACT_FORM_FROM,
			to: process.env.CONTACT_FORM_TO, // An array if you have multiple recipients.
			// cc: '',
			// bcc: '',
			subject: 'New form submission!',
			'h:Reply-To': email,
			html: `
			<p>
			<strong>Demo Requested:</strong> ${ demo }<br />
			<strong>First Name:</strong> ${ firstName }<br />
			<strong>Company:</strong> ${ company }<br />
			<strong>Email:</strong> ${ email }<br />
			</p>
			<hr />
			<p>This email was sent via form submission.</p>
			`,
			// text: '....', // plain text emails
		},
		( err, info ) => {
			console.log( err ? `Error: ${ err }` : `Response: ${ info }` );
		},
	);
};
