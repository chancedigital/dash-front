import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NoSSR from 'react-no-ssr';
import fetch from 'isomorphic-unfetch';
import Link from 'next/link';
import { CSSTransition } from 'react-transition-group';
import { uniqueId } from 'lodash';
import { Config } from 'root/config';
import Overview from 'molecules/overview';
import Crumbs from 'molecules/crumbs';
import DisclosuresNav from 'organisms/disclosures-nav';
import FooterInner from 'organisms/footer-inner';
import FullScreenFrame from 'organisms/full-screen-frame';
import Layout from 'organisms/layout';
import ProductCategory from 'organisms/product-category';
import Scroller from 'organisms/scroller';
import breakpoints from 'util/breakpoints';
import DownAnchor from 'atoms/down-anchor';
import { DeviceContext } from 'context/device';

class Platform extends Component {
	static async getInitialProps() {
		const categoryRes = await fetch(
			`${ Config.apiUrl }/wp-json/wp/v2/product_category`,
		);
		const categories = await categoryRes.json();
		const productRes = await fetch(
			`${ Config.apiUrl }/wp-json/wp/v2/product?_embed&per_page=20`,
		);
		const productPosts = await productRes.json();
		const pageRes = await fetch(
			`${ Config.apiUrl }/wp-json/dash/v1/page?slug=platform`,
		);
		const page = await pageRes.json();

		return {
			categories,
			productPosts,
			page,
		};
	}

	state = {
		active: 0,
		sectionWidth: 0,
		sectionHeight: 0,
	};

	updateActive = active => {
		const { page, categories } = this.props;
		const sections = [ page, ...categories ];

		if (
			sections && // component has children
			this.state.active !== active && // active state is different than updated active state
			active >= 0 && // not negative
			active < sections.length // not higher than number of sections
		) {
			this.setState( { active } );
		}
	};

	renderRightFooterComponent = () => {
		const { active } = this.state;
		return (
			<DeviceContext.Consumer>
				{ device => (
					<div className="footer__anchor footer__anchor--right">
						<CSSTransition
							in={ device === 'mobile' || active > 0 }
							key="demo-request"
							timeout={ 1000 }
							classNames="footer__link-"
							appear={ true }
							unmountOnExit
						>
							<Link href={ '/request-demo' } prefetch>
								<a className="footer__link footer__link--animated footer__link--anchor">
									Request Demo
									<span className="footer__link-arrow" aria-hidden />
								</a>
							</Link>
						</CSSTransition>
						<CSSTransition
							in={ device === 'desktop' && active === 0 }
							key="scroll-button"
							timeout={ 1000 }
							classNames="footer__link-"
							appear={ true }
							unmountOnExit
						>
							<DownAnchor
								onClick={ () =>
									this.setState( state => ( { active: ++state.active } ) )
								}
								className="footer__link footer__link--animated footer__down-anchor"
							/>
						</CSSTransition>
					</div>
				) }
			</DeviceContext.Consumer>
		);
	};

	render() {
		const { page, productPosts, categories, screenWidth } = this.props;
		const activeLabels = [ 'Overview', ...categories.map( cat => cat.name ) ];

		return (
			<Layout
				className="page-platform"
				title="Our Platform | Dash"
				bgVideo="https://res.cloudinary.com/chancedigital/video/upload/v1537475517/dash/video_platform.mp4"
			>
				<FullScreenFrame>
					<Scroller
						active={ this.state.active }
						onActiveChange={ this.updateActive }
						startAtBreakpoint={ breakpoints.medium }
					>
						{ page &&
							page.acf &&
							page.acf.overview && (
								<section className="page-platform__section">
									<Overview
										heading="01. Platform"
										content={ page.acf.overview }
									/>
								</section>
							) }

						{ categories &&
							categories.length > 0 &&
							categories.map( ( cat, i ) => (
								<section
									className="page-platform__section page-platform__section--product"
									key={ i }
								>
									<ProductCategory
										category={ cat }
										products={ productPosts.filter( post =>
											post.product_category.includes( cat.id ),
										) }
									/>
								</section>
							) ) }
					</Scroller>
				</FullScreenFrame>
				<FooterInner
					leftComponent={
						<Crumbs
							pageName="Platform"
							labels={ activeLabels }
							onDotClick={ this.updateActive }
							active={ this.state.active }
						/>
					}
					rightComponent={ this.renderRightFooterComponent() }
				/>
				<DeviceContext.Consumer>
					{ device =>
						device !== 'mobile' && (
							<NoSSR>
								<CSSTransition
									in={ this.state.active === 0 }
									key="disclosures"
									timeout={ 1000 }
									classNames="disclosures-nav-"
									appear={ true }
									unmountOnExit
								>
									<DisclosuresNav />
								</CSSTransition>
							</NoSSR>
						)
					}
				</DeviceContext.Consumer>
			</Layout>
		);
	}
}

export default Platform;

Platform.propTypes = {
	page: PropTypes.object.isRequired,
};
