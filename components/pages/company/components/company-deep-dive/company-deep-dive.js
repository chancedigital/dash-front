import React from 'react';
import PropTypes from 'prop-types';
import ReactResizeDetector from 'react-resize-detector';
import { CSSTransition } from 'react-transition-group';
import DynamicText from 'atoms/dynamic-text';
import Scroller from 'organisms/scroller';
import Swiper from 'organisms/swiper';

const renderSections = sections =>
	sections.map( ( { content, id } ) => (
		<div className="page-company__deep-content" key={ id }>
			<DynamicText content={ content } />
		</div>
	) );

const CompanyDeepDive = React.forwardRef(
	(
		{
			heading,
			sections,
			device,
			onActiveChange,
			onResize,
			active,
			screenWidth,
		},
		ref,
	) => (
		<ReactResizeDetector handleWidth handleHeight onResize={ onResize }>
			{ () => (
				<div ref={ ref } className="page-company__deep-wrapper">
					<CSSTransition
						in={ true }
						timeout={ 1000 }
						appear={ true }
						classNames="page-company__deep-heading-"
					>
						<h2 className="page-company__deep-heading">{ heading }</h2>
					</CSSTransition>
					{ device === 'desktop' ? (
						<Scroller
							ignoreScroll={ true }
							styleLeavingSlide={ true }
							active={ active }
							onActiveChange={ onActiveChange }
						>
							{ renderSections( sections ) }
						</Scroller>
					) : (
						<Swiper
							swipeMode="horizontal"
							screenWidth={ screenWidth }
							active={ active }
							onActiveChange={ onActiveChange }
						>
							{ renderSections( sections ) }
						</Swiper>
					) }
				</div>
			) }
		</ReactResizeDetector>
	),
);

CompanyDeepDive.propTypes = {
	sections: PropTypes.arrayOf(
		PropTypes.shape( {
			content: PropTypes.string.isRequired,
			id: PropTypes.oneOfType( [ PropTypes.number, PropTypes.string ] )
				.isRequired,
		} ),
	).isRequired,
	device: PropTypes.string.isRequired,
	onActiveChange: PropTypes.func.isRequired,
	onResize: PropTypes.func.isRequired,
	active: PropTypes.number.isRequired,
	screenWidth: PropTypes.number.isRequired,
};

export default CompanyDeepDive;
