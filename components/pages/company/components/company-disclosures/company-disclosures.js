import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import DisclosuresNav from 'organisms/disclosures-nav';

const CompanyDisclosures = ( { in: isIn } ) => (
	<CSSTransition
		in={ isIn }
		key="disclosures"
		timeout={ 1000 }
		classNames="disclosures-nav-"
		appear={ true }
		unmountOnExit
	>
		<DisclosuresNav />
	</CSSTransition>
);

CompanyDisclosures.propTypes = {
	in: PropTypes.bool.isRequired,
};

export default CompanyDisclosures;
