import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import DotNav from 'molecules/dot-nav';

const CompanyDeepDiveNav = ( {
	in: isIn,
	sections,
	onUpdateActive,
	active,
} ) => (
	<CSSTransition
		in={ isIn }
		key="deep"
		timeout={ 1000 }
		classNames="page-company__dot-nav-"
		appear={ true }
		unmountOnExit
	>
		<DotNav
			className="page-company__dot-nav--deep-dive page-company__dot-nav"
			sections={ sections }
			onDotClick={ onUpdateActive }
			active={ active }
		/>
	</CSSTransition>
);

CompanyDeepDiveNav.propTypes = {
	in: PropTypes.bool.isRequired,
	sections: PropTypes.array.isRequired,
	onUpdateActive: PropTypes.func.isRequired,
	active: PropTypes.number.isRequired,
};

export default CompanyDeepDiveNav;
