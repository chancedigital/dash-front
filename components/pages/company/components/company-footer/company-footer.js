import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import DownAnchor from 'atoms/down-anchor';
import Crumbs from 'molecules/crumbs';
import FooterInner from 'organisms/footer-inner';

const CompanyFooter = ( {
	pageName,
	crumbLabels,
	onCrumbSelect,
	onArrowClick,
	active,
	device,
} ) => {
	const renderRightFooterComponent = () => {
		return (
			<div className="footer__anchor footer__anchor--right">
				<CSSTransition
					in={ device === 'desktop' }
					key="scroll-button"
					timeout={ 1000 }
					classNames="footer__link-"
					appear={ true }
					unmountOnExit
				>
					<DownAnchor
						onClick={ onArrowClick }
						className="footer__link footer__link--animated footer__down-anchor"
						direction={ active === 2 ? 'up' : 'down' }
					/>
				</CSSTransition>
			</div>
		);
	};

	return (
		<FooterInner
			leftComponent={
				<Crumbs
					pageName={ pageName }
					labels={ crumbLabels }
					onDotClick={ onCrumbSelect }
					active={ active }
				/>
			}
			rightComponent={ renderRightFooterComponent() }
		/>
	);
};

CompanyFooter.propTypes = {
	pageName: PropTypes.string.isRequired,
	device: PropTypes.string.isRequired,
	crumbLabels: PropTypes.arrayOf( PropTypes.string ).isRequired,
	onCrumbSelect: PropTypes.func.isRequired,
	onArrowClick: PropTypes.func.isRequired,
	active: PropTypes.number.isRequired,
};

export default CompanyFooter;
