export const deepDiveContent = [
	{
		id: 0,
		content: `Steeped in innovation, education and craftsmanship.`,
	},
	{
		id: 1,
		content: `With a passion to embrace change, and a relentless focus.`,
	},
	{
		id: 2,
		content: `To deliver beautifully simple solutions to the institutional execution community.`,
	},
	{
		id: 3,
		content: `With incredible performance … however they define it.`,
	},
	{
		id: 4,
		content: `With Dash, you know.`,
	},
];
