import React, { Component } from 'react';
import PropTypes from 'prop-types';
import fetch from 'isomorphic-unfetch';
import { isNil } from 'lodash';
import Overview from 'molecules/overview';
import Layout from 'organisms/layout';
import TeamSection from 'organisms/team-section';
import FullScreenFrame from 'organisms/full-screen-frame';
import Scroller from 'organisms/scroller';
import CompanyFooter from './components/company-footer';
import CompanyDeepDive from './components/company-deep-dive';
import CompanyDeepDiveNav from './components/company-deep-dive-nav';
import CompanyDisclosures from './components/company-disclosures';
import breakpoints from 'util/breakpoints';
import { deepDiveContent } from './data';
import { Config } from 'root/config';
import { DeviceContext } from 'context/device';
const { apiUrl } = Config;

const INTERVAL = 6000;

/**
 * Company page component.
 */
class Company extends Component {
	/**
	 * Fetch the page data.
	 */
	static async getInitialProps() {
		const pageRes = await fetch(
			`${ apiUrl }/wp-json/dash/v1/page?slug=company`,
		);
		const teamRes = await fetch(
			`${ apiUrl }/wp-json/wp/v2/team?_embed&per_page=20`,
		);
		const page = await pageRes.json();
		const teamPosts = await teamRes.json();
		const deepDiveSections = deepDiveContent;

		return {
			page,
			teamPosts,
			deepDiveSections,
		};
	}

	/**
	 * Component state.
	 */
	state = {
		activeSection: 0,
		activeDeepSection: 0,
		sectionWidth: 0,
		sectionHeight: 0,
	};

	_interval = null;

	sectionWrapper = React.createRef();

	/**
	 * Prop types.
	 */
	static propTypes = {
		page: PropTypes.object.isRequired,
		teamPosts: PropTypes.arrayOf( PropTypes.object ),
	};

	/**
	 * Get the section labels for breadcrumbs.
	 */
	getSectionLabels = () => {
		const { page, teamPosts, deepDiveSections } = this.props;
		const overview =
			page && page.acf && page.acf.overview ? [ 'Overview' ] : [];
		const teamSection =
			teamPosts && teamPosts.length > 0 ? [ 'Management' ] : [];
		const deepDive =
			deepDiveSections && deepDiveSections.length ? [ 'Who We Are' ] : [];
		return [ ...overview, ...teamSection, ...deepDive ];
	};

	/**
	 * Handle resize of the section wrapper.
	 */
	onResize = ( sectionWidth, sectionHeight ) => {
		if ( isNil( this.state.sectionHeight ) ) {
			this.sectionWrapper.current.style.height =
				this.state.sectionHeight + 'px';
		}
		this.sectionWrapper.current.style.height = sectionHeight + 'px';
		this.sectionWrapper.current.removeAttribute( 'style' );
		this.setState( { sectionWidth, sectionHeight } );
		this.sectionWrapper.current.style.height = sectionHeight + 'px';
	};

	/**
	 * Update the active slide.
	 */
	updateActive = activeSection => {
		const sections = this.getSectionLabels();
		if (
			sections && // component has children
			activeSection >= 0 && // not negative
			activeSection < sections.length // not higher than number of sections
		) {
			this.setState(
				state =>
					state.activeSection !== activeSection && {
						activeSection,
						activeDeepSection: 0,
					},
			);
		}
	};

	updateActiveDeep = ( activeDeepSection, reset = false ) => {
		const { deepDiveSections } = this.props;
		if (
			deepDiveSections &&
			activeDeepSection >= 0 && // not negative
			activeDeepSection < deepDiveSections.length // not higher than number of sections
		) {
			this.setState(
				state =>
					state.activeDeepSection !== activeDeepSection && {
						activeDeepSection,
					},
				() => {
					if ( reset ) {
						clearInterval( this._interval );
						this._interval = null;
						this._interval = setInterval( this.doThatThang, INTERVAL );
					}
				},
			);
		}
	};

	componentDidMount() {
		const { deepDiveSections } = this.props;
		if ( deepDiveSections && deepDiveSections.length && ! this._interval ) {
			this._interval = setInterval( this.doThatThang, INTERVAL );
		}
	}

	componentDidUpdate( prevProps, prevState ) {
		const { deepDiveSections } = this.props;
		const { activeDeepSection, activeSection } = this.state;
		if ( prevState.activeSection !== activeSection ) {
			this.setState( { activeDeepSection: 0 } );
		}
		if ( deepDiveSections && deepDiveSections.length ) {
			if ( ! this._interval ) {
				this._interval = setInterval( this.doThatThang, INTERVAL );
			} else if ( activeDeepSection === deepDiveSections.length - 1 ) {
				clearInterval( this._interval );
				this._interval = null;
				this._interval = setInterval( this.doThatThang, INTERVAL );
			} else if ( activeDeepSection === 0 ) {
				clearInterval( this._interval );
				this._interval = null;
				this._interval = setInterval( this.doThatThang, INTERVAL );
			}
		} else if ( this._interval ) {
			clearInterval( this._interval );
			this._interval = null;
			this.setState( { activeDeepSection: 0 } );
		}
	}

	componentWillUnmount() {
		if ( this._interval ) {
			clearInterval( this._interval );
		}
	}

	doThatThang = () => {
		const { deepDiveSections } = this.props;
		if ( deepDiveSections && deepDiveSections.length ) {
			this.setState( state => {
				const activeDeepSection =
					state.activeDeepSection < deepDiveSections.length - 1
						? state.activeDeepSection + 1
						: 0;
				return { activeDeepSection };
			} );
		}
	};

	onAnchorArrowClick = () =>
		this.setState( state => {
			return state.activeSection === 2
				? { activeSection: --state.activeSection }
				: { activeSection: ++state.activeSection };
		} );

	/**
	 * Render the component.
	 */
	render() {
		const { page, teamPosts, screenWidth, deepDiveSections } = this.props;
		const { activeSection, activeDeepSection } = this.state;
		const overview = page.acf ? page.acf.overview : '';

		return (
			<DeviceContext.Consumer>
				{ device => (
					<Layout
						className="page-company"
						title="Our Company | Dash"
						bgVideo="https://res.cloudinary.com/chancedigital/video/upload/v1537475505/dash/video_company.mp4"
					>
						<FullScreenFrame>
							<Scroller
								active={ activeSection }
								onActiveChange={ this.updateActive }
								startAtBreakpoint={ breakpoints.medium }
							>
								<section className="page-company__section">
									<Overview heading="02. Company" content={ overview } />
								</section>

								<section className="page-company__section page-company__section--team">

									<TeamSection
										teamPosts={ teamPosts }
										isActive={ activeSection === 1 }
									/>
								</section>

								<section className="page-company__section page-company__section--deep-dive">
									<CompanyDeepDive
										ref={ this.sectionWrapper }
										//heading={ 'A culture of transparency' }
										heading={ 'At Dash we foster a culture born out of transparency' }
										sections={ deepDiveSections }
										device={ device }
										active={ activeDeepSection }
										screenWidth={ screenWidth }
										onActiveChange={ this.updateActiveDeep }
										onResize={ this.onResize }
									/>

									{ /* <DeepDive
										screenWidth={ screenWidth }
										sections={ deepDiveSections }
										isActive={ activeSection === 2 }
									/> */ }
								</section>
							</Scroller>
						</FullScreenFrame>

						{ device !== 'mobile' && (
							<CompanyDisclosures in={ activeSection === 0 } />
						) }

						<CompanyDeepDiveNav
							in={ activeSection === 2 || device === 'mobile' }
							sections={ deepDiveSections }
							onUpdateActive={ activeDeepSection =>
								this.updateActiveDeep( activeDeepSection, true )
							}
							active={ activeDeepSection }
						/>

						<CompanyFooter
							device={ device }
							pageName={ 'Company' }
							crumbLabels={ this.getSectionLabels() }
							onCrumbSelect={ this.updateActive }
							onArrowClick={ this.onAnchorArrowClick }
							active={ this.state.activeSection }
						/>
					</Layout>
				) }
			</DeviceContext.Consumer>
		);
	}
}

export default Company;
