import Layout from 'organisms/layout';
import React, { Component } from 'react';
import fetch from 'isomorphic-unfetch';
import Error from 'next/error';
import { Config } from 'root/config';

class Post extends Component {
	static async getInitialProps( context ) {
		const { slug, apiRoute } = context.query;
		const res = await fetch(
			`${ Config.apiUrl }/wp-json/dash/v1/${ apiRoute }?slug=${ slug }`,
		);
		const post = await res.json();
		return { post };
	}

	render() {
		const { post } = this.props;

		if ( ! post.title ) return <Error statusCode={ 404 } />;

		return (
			<Layout
				title={
					post.title
						? `${ post.title.rendered } | Our Team | Dash`
						: 'Our Team | Dash'
				}
			>
				<h1>{ post.title.rendered }</h1>
				<div
					dangerouslySetInnerHTML={ {
						__html: post.content.rendered,
					} }
				/>
			</Layout>
		);
	}
}

export default Post;
