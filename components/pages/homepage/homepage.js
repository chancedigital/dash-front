import React, { Component } from 'react';
import fetch from 'isomorphic-unfetch';
import PropTypes from 'prop-types';
import ReactResizeDetector from 'react-resize-detector';
import { CSSTransition } from 'react-transition-group';
import { isNil } from 'lodash';
import Layout from 'organisms/layout';
import Footer from 'organisms/footer';
import FullScreenFrame from 'organisms/full-screen-frame';
import Scroller from 'organisms/scroller';
import Swiper from 'organisms/swiper';
import DotNav from 'molecules/dot-nav';
import DynamicText from 'atoms/dynamic-text';
import breakpoints from 'util/breakpoints';
import { Config } from 'root/config';
import { DeviceContext } from 'context/device';

const INTERVAL = 6000;

/**
 * Homepage component.
 */
class Index extends Component {
	/**
	 * Fetch the page data.
	 */
	static async getInitialProps() {
		const pageRes = await fetch(
			`${ Config.apiUrl }/wp-json/dash/v1/page?slug=page_on_front`,
		);
		const page = await pageRes.json();
		const postsRes = await fetch(
			`${ Config.apiUrl }/wp-json/wp/v2/posts?_embed`,
		);
		const posts = await postsRes.json();
		const pagesRes = await fetch(
			`${ Config.apiUrl }/wp-json/wp/v2/pages?_embed`,
		);
		const pages = await pagesRes.json();
		return {
			page,
			posts,
			pages,
		};
	}

	_interval = null;

	/**
	 * Component state.
	 */
	state = {
		activeSection: 0,
		sectionWidth: 0,
		sectionHeight: 0,
	};

	getPageSections = () =>
		this.props.page && this.props.page.acf
			? this.props.page.acf.scroll_sections
			: [];

	hasPageSections = () => {
		const sections = this.getPageSections();
		return !! ( sections && sections.length );
	};

	/**
	 * Update the active slide.
	 */
	updateActive = activeSection => {
		const sections = this.getPageSections();
		if (
			this.hasPageSections() && // component has children
			this.state.activeSection !== activeSection && // active state is different than updated active state
			activeSection >= 0 && // not negative
			activeSection < sections.length // not higher than number of sections
		) {
			this.setState( { activeSection } );
			if ( this._interval ) {
				clearInterval( this._interval );
				this._interval = null;
				this._interval = setInterval( this.doThatThang, INTERVAL );
			}
		}
	};

	/**
	 * Ref for the section wrapper DOM element.
	 */
	sectionWrapper = React.createRef();

	/**
	 * Handle resize of the section wrapper.
	 */
	onResize = ( sectionWidth, sectionHeight ) => {
		if ( isNil( this.state.sectionHeight ) ) {
			this.sectionWrapper.current.style.height =
				this.state.sectionHeight + 'px';
		}
		this.sectionWrapper.current.style.height = sectionHeight + 'px';
		this.sectionWrapper.current.removeAttribute( 'style' );
		this.setState( { sectionWidth, sectionHeight } );
		this.sectionWrapper.current.style.height = sectionHeight + 'px';
	};

	doThatThang = () => {
		const sections = this.getPageSections();
		if ( this.hasPageSections() ) {
			this.setState( state => {
				const activeSection =
					state.activeSection < sections.length - 1
						? state.activeSection + 1
						: 0;
				return { activeSection };
			} );
		}
	};

	componentDidUpdate( prevProps, prevState ) {
		const sections = this.getPageSections();
		const { activeSection } = this.state;
		if ( this.hasPageSections() ) {
			if ( ! this._interval ) {
				this._interval = setInterval( this.doThatThang, INTERVAL );
			} else if ( activeSection === sections.length - 1 ) {
				clearInterval( this._interval );
				this._interval = null;
				this._interval = setInterval( this.doThatThang, INTERVAL * 2 );
			} else if ( activeSection === 0 ) {
				clearInterval( this._interval );
				this._interval = null;
				this._interval = setInterval( this.doThatThang, INTERVAL );
			}
		} else if ( this._interval ) {
			clearInterval( this._interval );
			this._interval = null;
			this.setState( { activeSection: 0 } );
		}
	}

	componentDidMount() {
		if ( this.hasPageSections() && ! this._interval ) {
			this._interval = setInterval( this.doThatThang, INTERVAL );
		}
	}

	componentWillUnmount() {
		if ( this._interval ) {
			clearInterval( this._interval );
		}
	}

	/**
	 * Render the component.
	 */
	render() {
		const { page, screenWidth } = this.props;

		const { activeSection } = this.state;
		const sectionHeading = page && page.acf ? page.acf.section_heading : '';
		const sections = page && page.acf ? page.acf.scroll_sections : [];

		return (
			<DeviceContext.Consumer>
				{ device => (
					<Layout
						className="homepage"
						title="Welcome to Dash"
						bgVideo="https://res.cloudinary.com/chancedigital/video/upload/v1537369900/dash/video_home.mp4"
					>
						<FullScreenFrame>
							{ sections &&
								sections.length > 0 && (
									<ReactResizeDetector
										handleWidth
										handleHeight
										onResize={ this.onResize }
									>
										{ ( width, height ) => (
											<section
												ref={ this.sectionWrapper }
												className="homepage__section"
											>
												{ sectionHeading && (
													<CSSTransition
														in={ true }
														timeout={ 1000 }
														appear={ true }
														classNames="homepage__heading-"
													>
														<h2 className="homepage__heading">
															{ sectionHeading }
														</h2>
													</CSSTransition>
												) }
												{ device === 'desktop' ? (
													<Scroller
														styleLeavingSlide={ true }
														active={ activeSection }
														onActiveChange={ this.updateActive }
													>
														{ sections.map( ( { content }, i ) => (
															<div key={ i } className="homepage__content">
																<DynamicText content={ content } />
															</div>
														) ) }
													</Scroller>
												) : (
													<Swiper
														swipeMode="horizontal"
														screenWidth={ screenWidth }
														active={ activeSection }
														onActiveChange={ this.updateActive }
													>
														{ sections.map( ( { content }, i ) => (
															<div key={ i } className="homepage__content">
																<DynamicText content={ content } />
															</div>
														) ) }
													</Swiper>
												) }
											</section>
										) }
									</ReactResizeDetector>
								) }
						</FullScreenFrame>
						<CSSTransition
							in={ true }
							timeout={ 1000 }
							appear={ true }
							classNames="homepage__dot-nav-"
						>
							<DotNav
								layout={ device === 'mobile' ? 'horizontal' : 'vertical' }
								className="homepage__dot-nav"
								sections={ sections }
								onDotClick={ this.updateActive }
								active={ activeSection }
							/>
						</CSSTransition>
						<Footer screenWidth={ screenWidth } />
					</Layout>
				) }
			</DeviceContext.Consumer>
		);
	}
}

export default Index;
