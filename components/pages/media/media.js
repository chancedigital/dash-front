import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NoSSR from 'react-no-ssr';
import fetch from 'isomorphic-unfetch';
import { CSSTransition } from 'react-transition-group';
import DownAnchor from 'atoms/down-anchor';
import Layout from 'organisms/layout';
import PostGrid from 'organisms/post-grid';
import FullScreenFrame from 'organisms/full-screen-frame';
import Scroller from 'organisms/scroller';
import Overview from 'molecules/overview';
import FooterInner from 'organisms/footer-inner';
import DisclosuresNav from 'organisms/disclosures-nav';
import Crumbs from 'molecules/crumbs';
import handlePostData from 'util/handle-post-data';
import breakpoints from 'util/breakpoints';
import { Config } from 'root/config';
import { DeviceContext } from 'context/device';

/**
 * Media page component.
 */
class Media extends Component {
	/**
	 * Fetch the page data.
	 */
	static async getInitialProps( { query } ) {
		const { activeSection = 0 } = query;
		const pageRes = await fetch(
			`${ Config.apiUrl }/wp-json/dash/v1/page?slug=media`,
		);
		const page = await pageRes.json();
		const postsRes = await fetch(
			`${ Config.apiUrl }/wp-json/wp/v2/posts?_embed&per_page=99`,
		);
		const posts = await postsRes.json();
		const catRes = await fetch( `${ Config.apiUrl }/wp-json/wp/v2/categories` );
		const cats = await catRes.json();
		const navItems = [
			{
				label: 'News',
			},
			...( ( await posts ) && posts.length > 0
				? posts
						.map( post => new Date( post.date ).getFullYear() ) // Array of post date years
						.filter( ( post, i, arr ) => arr.indexOf( post ) === i ) // Filter our duplicates
						.map( year => ( { label: String( year ) } ) )
				: [] ),
			...( ( await cats ) && cats.length > 0
				? cats
						.map( cat => ( {
							label: cat.name,
							id: cat.id,
							subitems: [],
						} ) )
						.filter( cat => cat.id !== 1 ) // uncategorized
						.reverse()
				: [] ),
		];

		return {
			page,
			posts: handlePostData( posts ),
			navItems,
			activeSection: parseInt( activeSection ),
		};
	}

	/**
	 * Component state.
	 */
	state = {
		activeSection: 0,
	};

	/**
	 * Get the section labels for breadcrumbs.
	 */
	getSectionLabels = () => {
		const { page, posts } = this.props;
		const overview =
			page && page.acf && page.acf.overview ? [ 'Overview' ] : [];
		const postsSection = posts && posts.length > 0 ? [ 'All Articles' ] : [];
		return [ ...overview, ...postsSection ];
	};

	/**
	 * Update the active slide.
	 */
	updateActive = activeSection => {
		const sections = this.getSectionLabels();
		if (
			sections && // component has children
			this.state.activeSection !== activeSection && // active state is different than updated active state
			activeSection >= 0 && // not negative
			activeSection < sections.length // not higher than number of sections
		) {
			this.setState( { activeSection } );
		}
	};

	componentDidMount() {
		const { activeSection } = this.props;
		if ( activeSection ) {
			this.setState( {
				activeSection,
			} );
		}
	}

	renderRightFooterComponent = () => {
		const { activeSection } = this.state;
		return (
			<DeviceContext.Consumer>
				{ device => (
					<div className="footer__anchor footer__anchor--right">
						<CSSTransition
							in={ device === 'desktop' }
							key="scroll-button"
							timeout={ 1000 }
							classNames="footer__link-"
							appear={ true }
							unmountOnExit
						>
							<DownAnchor
								onClick={ () =>
									this.setState( state => {
										return activeSection === 1
											? { activeSection: --state.activeSection }
											: { activeSection: ++state.activeSection };
									} )
								}
								className="footer__link footer__link--animated footer__down-anchor"
								direction={ activeSection === 1 ? 'up' : 'down' }
							/>
						</CSSTransition>
					</div>
				) }
			</DeviceContext.Consumer>
		);
	};

	/**
	 * Render the component.
	 */
	render() {
		const { page, posts, navItems, screenWidth } = this.props;
		const { activeSection } = this.state;

		return (
			<Layout
				className="page-media"
				title="Media | Dash"
				bgVideo="https://res.cloudinary.com/chancedigital/video/upload/v1537475510/dash/video_media.mp4"
			>
				<FullScreenFrame>
					<Scroller
						active={ activeSection }
						onActiveChange={ this.updateActive }
						startAtBreakpoint={ breakpoints.medium }
					>
						{ page &&
							page.acf &&
							page.acf.overview && (
								<section className="page-media__section">
									<Overview heading="03. Media" content={ page.acf.overview } />
								</section>
							) }

						{ posts &&
							posts.length > 0 && (
								<section className="page-media__section page-media__section--articles">
									<PostGrid
										isActive={ activeSection === 1 }
										posts={ posts }
										navItems={ navItems }
									/>
								</section>
							) }
					</Scroller>
				</FullScreenFrame>

				<FooterInner
					leftComponent={
						<Crumbs
							pageName="Media"
							labels={ this.getSectionLabels() }
							onDotClick={ this.updateActive }
							active={ activeSection }
						/>
					}
					rightComponent={ this.renderRightFooterComponent() }
				/>
				<DeviceContext.Consumer>
					{ device =>
						device !== 'mobile' && (
							<NoSSR>
								<CSSTransition
									in={ activeSection === 0 }
									key="disclosures"
									timeout={ 1000 }
									classNames="disclosures-nav-"
									appear={ true }
									unmountOnExit
								>
									<DisclosuresNav />
								</CSSTransition>
							</NoSSR>
						)
					}
				</DeviceContext.Consumer>
			</Layout>
		);
	}
}

export default Media;

Media.propTypes = {
	page: PropTypes.object.isRequired,
	posts: PropTypes.arrayOf( PropTypes.object ),
	navItems: PropTypes.array,
};
