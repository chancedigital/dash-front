import React, { Component } from 'react';
import Link from 'next/link';
import NoSSR from 'react-no-ssr';
import { CSSTransition } from 'react-transition-group';
import fetch from 'isomorphic-unfetch';
import Layout from 'organisms/layout';
import DisclosuresNav from 'organisms/disclosures-nav';
import FooterInner from 'organisms/footer-inner';
import RequestDemoForm from './components/request-demo-form';
import { Config } from 'root/config';
import { DeviceContext } from 'context/device';
// import { createForm, createFactory, createField } from 'micro-form';

class RequestDemo extends Component {
	static async getInitialProps( { query } ) {
		const { demo = 'selectDemo' } = query;
		const productRes = await fetch(
			`${ Config.apiUrl }/wp-json/wp/v2/product?_embed&per_page=20`,
		);
		const productPosts = await productRes.json();
		return { productPosts, demo };
	}

	renderFooterButton = device => {
		return (
			<div className="footer__anchor footer__anchor--left">
				<Link href={ '/platform' } prefetch>
					<a className="footer__link footer__link--anchor">Go Back</a>
				</Link>
			</div>
		);
	};

	renderRightFooterComponent = () => {
		return (
			<DeviceContext.Consumer>
				{ device => (
					<div className="footer__anchor footer__anchor--right">
						<CSSTransition
							in={ device === 'mobile' }
							key="go-back"
							timeout={ 1000 }
							classNames="footer__link-"
							appear={ true }
							unmountOnExit
						>
							<Link href={ '/platform' } prefetch>
								<a className="footer__link footer__link--animated footer__link--anchor">
									Go Back
									<span className="footer__link-arrow" aria-hidden />
								</a>
							</Link>
						</CSSTransition>
					</div>
				) }
			</DeviceContext.Consumer>
		);
	};

	render() {
		const { productPosts } = this.props;
		return (
			<DeviceContext.Consumer>
				{ device => (
					<Layout
						className="page-request-demo"
						title="Request a Demo | Dash"
						bgImg="/static/images/bg_demo.jpg"
					>
						<main className="page-request-demo__wrapper">
							<section className="page-request-demo__section">
								<h2 className="page-request-demo__heading">Request Demo</h2>
								<div>
									<RequestDemoForm productPosts={ productPosts } />
								</div>
							</section>
						</main>

						<FooterInner
							leftComponent={
								device === 'desktop' ? this.renderFooterButton( device ) : null
							}
							rightComponent={ this.renderRightFooterComponent() }
						/>
						{ /* { device !== 'mobile' && (
							<NoSSR>
								<CSSTransition
									in={ true }
									key="disclosures"
									timeout={ 1000 }
									classNames="disclosures-nav-"
									appear={ true }
									unmountOnExit
								>
									<DisclosuresNav />
								</CSSTransition>
							</NoSSR>
						) } */ }
					</Layout>
				) }
			</DeviceContext.Consumer>
		);
	}
}

export default RequestDemo;
