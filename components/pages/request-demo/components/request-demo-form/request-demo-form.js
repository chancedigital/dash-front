import React, { PureComponent } from 'react';
import classNames from 'classnames';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { camelCase } from 'lodash';
import fetch from 'isomorphic-unfetch';
import Link from 'next/link';
import Input from 'atoms/input';
import Select from 'atoms/select';

const FormSchema = Yup.object().shape( {
	demo: Yup.string()
		.required( 'Demo is required' )
		.nullable(),
	firstName: Yup.string()
		.min( 2, 'Too Short' )
		.max( 50, 'Too Long!' )
		.required( 'Name is required' ),
	company: Yup.string()
		.min( 2, 'Too Short!' )
		.max( 60, 'Too Long!' ),
	email: Yup.string()
		.email( 'Invalid email address' )
		.required( 'Email is required' ),
} );

class RequestDemoForm extends PureComponent {
	handleSubmit = ( data, { setSubmitting, setErrors, setStatus } ) => {
		fetch( '/api/v1/contact', {
			method: 'post',
			headers: {
				Accept: 'application/json, text/plain, */*',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify( data ),
		} )
			.then( res => {
				setStatus( { msg: 'Sending...' } );
				if ( res.status === 200 ) {
					setSubmitting( false );
					setStatus( { msg: 'Your submission has been sent!' } );
				} else {
					//
				}
			} )
			.catch( error => {
				setSubmitting( false );
				setErrors( error );
				setStatus( {
					msg:
						'There was an error submitting your form. Please try again later.',
				} );
			} );
	};

	getDemoOptions = () => {
		const { productPosts } = this.props;
		const defaultOption = {
			id: 0,
			value: 'selectDemo',
			label: 'Select Demo',
			props: { disabled: true },
		};
		const options =
			productPosts && productPosts.length > 0
				? productPosts.map( ( product, i ) => ( {
						id: i + 1,
						value: camelCase( product.title.rendered ),
						label: product.title.rendered,
				  } ) )
				: [];
		return [ defaultOption, ...options ];
	};

	_button = React.createRef();

	state = {
		buttonAngle: 0,
	};

	render() {
		return (
			<Formik
				initialValues={ {
					firstName: '',
					company: '',
					email: '',
				} }
				validationSchema={ FormSchema }
				onSubmit={ this.handleSubmit }
			>
				{ ( { isSubmitting, status, errors, touched, submitCount } ) => (
					<Form className="request-demo-form">
						<Select
							name="demo"
							label="Select Demo"
							//options={ this.getDemoOptions() }
							options={ [
								{
									id: 0,
									value: 'selectDemo',
									label: 'Select Demo',
									props: { disabled: true },
								},
								{ id: 1, value: 'execution', label: 'Execution' },
								{ id: 2, value: 'analytics', label: 'Analytics' },
								{
									id: 3,
									value: 'tradingTechnology',
									label: 'Trading Technology',
								},
								{ id: 4, value: 'regTech', label: 'RegTech' },
							] }
							defaultValue="selectDemo"
						/>
						<Input
							type="text"
							name="firstName"
							label="First Name"
							erorrs={ errors.firstName }
							touched={ touched.firstName }
							showRequiredStar
						/>
						<Input
							type="text"
							name="company"
							label="Company"
							erorrs={ errors.company }
							touched={ touched.company }
						/>
						<Input
							type="email"
							name="email"
							label="Email"
							erorrs={ errors.email }
							touched={ touched.email }
							showRequiredStar
						/>
						{ status &&
							status.msg && (
								<div className="message-group">{ status.msg }</div>
							) }
						<div className="submit-group">
							<span className="submit-group__phone">
								Or Call{' '}
								<Link href="tel:8445693200">
									<a style={ { color: 'inherit', textDecoration: 'none' } }>
										844-569-3200
									</a>
								</Link>
							</span>
							<button
								ref={ this._button }
								onClick={ () => this.setState( state => ! isSubmitting && ( {
									buttonAngle: state.buttonAngle + 720
								} ) ) }
								className={ classNames(
                                    'submit-group__button',
                                    'check-submit',
                                ) }
								type="submit"
								disabled={ isSubmitting }
								style={ {
									transform: `rotate( ${ this.state.buttonAngle }deg )`,
									WebkitTransform: `rotate( ${ this.state.buttonAngle }deg )`,
								} }
							>
								<span className="screen-reader-text">Submit</span>
							</button>
						</div>
					</Form>
				) }
			</Formik>
		);
	}
}

export default RequestDemoForm;
