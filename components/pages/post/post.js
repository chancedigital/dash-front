import React, { Component } from 'react';
import Link from 'next/link';
import NoSSR from 'react-no-ssr';
import { CSSTransition } from 'react-transition-group';
import ScrollBar from 'react-perfect-scrollbar';
import DisclosuresNav from 'organisms/disclosures-nav';
import Layout from 'organisms/layout';
import FooterInner from 'organisms/footer-inner';
import fetch from 'isomorphic-unfetch';
import Error from 'next/error';
import { Config } from 'root/config';
import formatDate from 'util/format-date';
import { DeviceContext } from 'context/device';

class Post extends Component {
	static async getInitialProps( { query } ) {
		const { slug = '' } = query;
		const res = await fetch(
			`${ Config.apiUrl }/wp-json/dash/v1/post?slug=${ slug }`,
		);
		const post = await res.json();
		return {
			post,
		};
	}

	render() {
		const { post } = this.props;
		const postDate = post ? formatDate( post.date ) : null;

		if ( ! post.title ) return <Error statusCode={ 404 } />;

		return (
			<Layout
				className="page-post"
				title={
					post.title ? `${ post.title.rendered } | Dash` : 'Media | Dash'
				}
			>
				<div className="page-post__post-wrapper">
					<ScrollBar component="article" className="page-post__post">
						<header className="page-post__header">
							<span className="page-post__meta">{ postDate }</span>
							<h1 className="page-post__title">{ post.title.rendered }</h1>
						</header>
						<div
							className="page-post__content"
							dangerouslySetInnerHTML={ {
								__html: post.content.rendered,
							} }
						/>
					</ScrollBar>
				</div>

				<FooterInner
					leftComponent={
						<div className="footer__anchor footer__anchor--left">
							<Link
								as={ `/media/posts` }
								href={ `/media?activeSection=1` }
								prefetch
							>
								<a className="footer__link footer__link--anchor">
									All Articles
									<span className="footer__link-arrow" aria-hidden />
								</a>
							</Link>
						</div>
					}
				/>
				{/* \ */}
			</Layout>
		);
	}
}

export default Post;
