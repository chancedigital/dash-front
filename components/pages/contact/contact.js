import React, { Component } from 'react';
import Link from 'next/link';
import NoSSR from 'react-no-ssr';
import { CSSTransition } from 'react-transition-group';
import { kebabCase } from 'lodash';
import classNames from 'classnames';
import DisclosuresNav from 'organisms/disclosures-nav';
import Layout from 'organisms/layout';
import FooterInner from 'organisms/footer-inner';
import SocialNav from 'molecules/social-nav';
import { contacts, locations, socialNav } from 'data/contact-info';
import { DeviceContext } from 'context/device';

class Contact extends Component {
	state = { activeLocation: 0 };

	setActiveLocation = activeLocation => {
		this.setState( { activeLocation } );
	};

	renderFooterButton = device => {
		return (
			<div className="footer__anchor footer__anchor--left">
				<Link href={ '/' } prefetch>
					<a className="footer__link footer__link--anchor">Go Home</a>
				</Link>
			</div>
		);
	};

	render() {
		const { activeLocation } = this.state;
		return (
			<DeviceContext.Consumer>
				{ device => (
					<Layout
						className="page-contact"
						title="Contact Us | Dash"
						bgOverlay={ locations.map( ( location, index ) => (
							<CSSTransition
								key={ location.id }
								in={ location.id === activeLocation }
								timeout={ 500 }
								appear={ true }
								classNames="page-contact__background-"
								unmountOnExit
							>
								<div
									role="img"
									aria-hidden="true"
									className={ classNames(
										'page-contact__background',
										`page-contact__background--${ kebabCase( location.name ) }`,
									) }
								/>
							</CSSTransition>
						) ) }
					>
						<main className="page-contact__wrapper">
							<section>
								<h2 className="page-contact__heading page-contact__heading--main">
									Contacts
								</h2>
								{ contacts &&
									contacts.length > 0 &&
									contacts.map( ( contact, i ) => (
										<div className="page-contact__contact-info" key={ i }>
											<h3 className="page-contact__contact-name">
												{ contact.name }
											</h3>
											<a
												href={ `mailto:${ contact.email }` }
												className="page-contact__contact-email"
											>
												{ contact.email }
											</a>
											<p className="page-contact__contact-phone">
												{ contact.phone }
											</p>
										</div>
									) ) }
							</section>
							<section>
								<h2 className="page-contact__heading">Locations</h2>
								<ul className="page-contact__location-list">
									{ locations &&
										locations.length > 0 &&
										locations.map( ( location, i ) => {
											const isActive = this.state.activeLocation === i;
											return (
												<li
													key={ i }
													className={
														isActive
															? 'page-contact__location page-contact__location--active'
															: 'page-contact__location'
													}
													onClick={ () => this.setActiveLocation( i ) }
												>
													<h3 className="page-contact__location-title">
														{ location.name }
													</h3>

													<CSSTransition
														in={ isActive }
														timeout={ device === 'desktop' ? 1000 : 0 }
														key={ i }
														appear={ true }
														classNames="page-contact__location-wrapper-"
														unmountOnExit
													>
														{ state => (
															<div className="page-contact__location-wrapper">
																<div className="page-contact__location-details">
																	<address
																		className="page-contact__location-address"
																		dangerouslySetInnerHTML={ {
																			__html: location.address,
																		} }
																	/>
																	<span className="page-contact__location-phone">
																		{ location.phone }
																	</span>
																</div>
															</div>
														) }
													</CSSTransition>
												</li>
											);
										} ) }
								</ul>
							</section>
						</main>
						<FooterInner
							leftComponent={
								device === 'desktop' ? this.renderFooterButton( device ) : null
							}
							rightComponent={ <SocialNav navItems={ socialNav } /> }
						/>
						{/* { device !== 'mobile' && (
							<NoSSR>
								<CSSTransition
									in={ true }
									key="disclosures"
									timeout={ 1000 }
									classNames="disclosures-nav-"
									appear={ true }
									unmountOnExit
								>
									<DisclosuresNav />
								</CSSTransition>
							</NoSSR>
						) } */}
					</Layout>
				) }
			</DeviceContext.Consumer>
		);
	}
}

export default Contact;
