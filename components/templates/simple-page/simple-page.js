import React, { Component } from 'react';
import Layout from 'organisms/layout';
import Header from 'organisms/header';
import Footer from 'organisms/footer';

class SimplePage extends Component {

	render() {
		const { title, content } = this.props;
		return (
			<Layout>
				<Header />
				<section>
					<h1>{ title }</h1>
					<div
						dangerouslySetInnerHTML={ {
							__html: content,
						} }
					/>
				</section>
				<Footer />
			</Layout>
		);
	}
}

export default SimplePage;
