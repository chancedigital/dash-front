import React from 'react';

const ScrollFrame = ( { children } ) => (
	<div className="scroll-frame">
		<div className="scroll-frame__inner">{ children }</div>
	</div>
);

export default ScrollFrame;
