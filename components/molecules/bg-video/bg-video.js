import React from 'react';
import { DeviceContext } from 'context/device';

const BgVideo = ( { src } ) => (
	<DeviceContext.Consumer>
		{ device =>
			device === 'desktop' && (
				<video
					className="bg-video"
					playsInline
					autoPlay
					muted
					loop
					src={ src }
				/>
			)
		}
	</DeviceContext.Consumer>
);

export default BgVideo;
