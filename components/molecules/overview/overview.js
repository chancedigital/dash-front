import React from 'react';
import { CSSTransition } from 'react-transition-group';

const Overview = ( { heading, content } ) => (
	<div className="overview">
		<CSSTransition
			in={ true }
			timeout={ 1000 }
			appear={ true }
			classNames="overview__heading-"
		>
			<h2 className="overview__heading">{ heading }</h2>
		</CSSTransition>
		<div
			className="overview__content"
			dangerouslySetInnerHTML={ {
				__html: content,
			} }
		/>
	</div>
);

export default Overview;
