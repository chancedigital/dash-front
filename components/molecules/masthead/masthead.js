import React, { Component } from 'react';
import Link from 'next/link';
import classNames from 'classnames';
import PropTypes from 'prop-types';

// Components
import DashLogo from 'atoms/dash-logo';
import Hamburger from 'atoms/hamburger';

class Masthead extends Component {
	_timeout = null;

	state = {
		entered: false,
	};

	static propTypes = {
		handleMenuChange: PropTypes.func.isRequired,
		menuIsOpen: PropTypes.bool,
	};

	static defaultProps = {
		menuIsOpen: false,
	};

	componentDidMount() {
		this.setState( { entered: true } );
		this._timeout = this.props.handleMenuChange;
	}

	componentWillUnmount() {
		clearTimeout( this._timeout );
	}

	render() {
		const { menuIsOpen, handleMenuChange, handleMenuClose } = this.props;
		const headerClass = classNames( {
			masthead: true,
			'masthead--in': this.state.entered,
		} );
		return (
			<header className={ headerClass }>
				<Link href="/" prefetch>
					<a
						className="masthead__link"
						onClick={ menuIsOpen ? handleMenuClose : null }
					>
						<div className="masthead__logo">
							<DashLogo />
						</div>
					</a>
				</Link>
				<Hamburger
					handleMenuChange={ handleMenuChange }
					menuIsOpen={ menuIsOpen }
				/>
			</header>
		);
	}
}

export default Masthead;
