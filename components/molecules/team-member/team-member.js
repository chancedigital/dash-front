import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TeamMember extends Component {
	static propTypes = {
		img: PropTypes.object,
		bio: PropTypes.string.isRequired,
		jobTitle: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
	};

	render() {
		const { img, bio, jobTitle, name } = this.props;
		const hasImg = img && img.source_url;
		return (
			<article
				className={ hasImg ? 'team-member' : 'team-member team-member--no-img' }
			>
				{ hasImg && (
					<div className="team-member__img">
						<img src={ img.source_url } alt={ img.alt_text } />
					</div>
				) }
				<div className="team-member__info">
					<h2 className="team-member__name">{ name }</h2>
					{ jobTitle && (
						<h3 className="team-member__job-title">{ jobTitle }</h3>
					) }
					<div
						className="team-member__bio"
						dangerouslySetInnerHTML={ {
							__html: bio,
						} }
					/>
				</div>
			</article>
		);
	}
}

export default TeamMember;
