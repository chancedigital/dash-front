import React, { Component } from 'react';
import Link from 'next/link';
import cx from 'classnames';
import MenuItem from './components/menu-item';

// Utilities
import getPageTemplate from 'util/page-templates';

class Menu extends Component {
	getSlug( url ) {
		const parts = url.split( '/' );
		return parts.length > 2 ? parts[ parts.length - 2 ] : '';
	}

	renderMenuItems = items => {
		const subMenu = children => {
			if ( children && children.length > 0 ) {
				if ( this.props.isOrderedList ) {
					return (
						<ol className="menu__submenu menu__submenu--ordered">
							{ this.renderMenuItems( children ) }
						</ol>
					);
				} else {
					return (
						<ul className="menu__submenu">
							{ this.renderMenuItems( children ) }
						</ul>
					);
				}
			}
		};

		const getChildren = parent =>
			items.filter( item => parent.db_id.toString() === item.menu_item_parent );

		return items
			.filter(
				( item, index, arr ) =>
					// Remove nested items from appearing in the same level as parents.
					item.menu_item_parent === arr[ 0 ].menu_item_parent,
			)
			.map( item => {
				const getActualPage = () => {
					switch ( item.object ) {
						case 'category':
							return 'category';
						case 'page':
							return getPageTemplate( this.getSlug( item.url ) );
						default:
							return 'post';
					}
				};
				const actualPage = getActualPage();
				return (
					<MenuItem
						key={ item.ID }
						subMenu={ subMenu( getChildren( item ) ) }
						handleMenuChange={ this.props.handleMenuChange }
						item={ item }
						actualPage={ actualPage }
						slug={ this.getSlug( item.url ) }
					/>
				);
			} );
	};

	render() {
		const navClass = cx( {
			menu: true,
			[ this.props.className ]: !! this.props.className,
		} );

		if ( this.props.isOrderedList ) {
			return (
				<nav className={ navClass }>
					<ol className="menu__list menu__list--ordered">
						{ this.renderMenuItems( this.props.menu.items ) }
					</ol>
				</nav>
			);
		} else {
			return (
				<nav className={ navClass }>
					<ul className="menu__list">
						{ this.renderMenuItems( this.props.menu.items ) }
					</ul>
				</nav>
			);
		}
	}
}

export default Menu;
