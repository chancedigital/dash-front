import React from 'react';
import MenuLink from '../menu-link';
import PropTypes from 'prop-types';

const MenuItem = ( { subMenu, handleMenuChange, item, actualPage, slug } ) => (
	<li className="menu__item">
		<MenuLink
			handleMenuChange={ handleMenuChange }
			actualPage={ actualPage }
			slug={ slug }
			{ ...item }
		/>
		{ subMenu }
	</li>
);

MenuItem.propTypes = {
	subMenu: PropTypes.node,
	handleMenuChange: PropTypes.func,
	item: PropTypes.object.isRequired,
	actualPage: PropTypes.string,
	slug: PropTypes.string.isRequired,
};

export default MenuItem;
