import React from 'react';
import Link from 'next/link';

const MenuLink = ( { handleMenuChange, target, title, object, url, actualPage, slug } ) => {
	const href = `/${ actualPage }?slug=${ slug }&apiRoute=${ object }`;
	const nextHref = object === 'page' ? `/${ slug }` : `/${ object }/${ slug }`;
	const anchor =
		target === '_blank' || object === 'custom' ? (
			<a className="menu__link" target={ target }>
				{ title }
			</a>
		) : (
			<a className="menu__link" onClick={ handleMenuChange }>
				{ title }
			</a>
		);

	if ( object === 'custom' ) {
		return <Link href={ url }>{ anchor }</Link>;
	}
	if ( target === '_blank' ) {
		return (
			<Link as={ nextHref } href={ href }>
				{ anchor }
			</Link>
		);
	}
	return (
		<Link href={ nextHref } prefetch>
			{ anchor }
		</Link>
	);
};

export default MenuLink;
