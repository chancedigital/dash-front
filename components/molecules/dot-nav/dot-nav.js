import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

const DotNav = ( { className, sections, onDotClick, active, layout } ) => {
	return (
		<nav
			className={ cx( 'dot-nav', className, {
				'dot-nav--horizontal': layout === 'horizontal',
			} ) }
		>
			<ul className="dot-nav__menu">
				{ sections &&
					sections.length > 0 &&
					sections.map( ( section, i ) => (
						<li className="dot-nav__item" key={ i }>
							<button
								className={
									active === i
										? 'dot-nav__button dot-nav__button--active'
										: 'dot-nav__button'
								}
								onClick={ () => onDotClick( i ) }
							>
								<span className="screen-reader-text">Go to page { i + 1 }</span>
							</button>
						</li>
					) ) }
			</ul>
		</nav>
	);
};

DotNav.propTypes = {
	layout: PropTypes.string,
	sections: PropTypes.array,
	active: PropTypes.number,
	onDotClick: PropTypes.func.isRequired,
};

DotNav.defaultProps = {
	layout: 'vertical',
	sections: [ 1 ],
	active: 0,
};

export default DotNav;
