import React from 'react';
import { kebabCase } from 'lodash';

const SocialNav = ( { navItems } ) => (
	<nav className="social-nav">
		<ul className="social-nav__menu">
			{ navItems &&
				navItems.length > 0 &&
				navItems.map( ( item, i ) => (
					<li
						className={ `social-nav__item social-nav__item--${ kebabCase(
							item.name,
						) }` }
						key={ i }
					>
						<a
							rel="noopener noreferrer"
							target="_blank"
							href={ item.link }
							className="social-nav__link"
						>
							<span className="screen-reader-text">{ item.name }</span>
						</a>
					</li>
				) ) }
		</ul>
	</nav>
);

export default SocialNav;
