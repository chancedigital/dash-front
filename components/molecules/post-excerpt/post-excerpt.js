import React from 'react';
import Link from 'next/link';
import formatDate from 'util/format-date';

const PostExcerpt = ( { post } ) => {
	const date = formatDate( post.date );
	return (
		<Link
			as={ `/media/${ post.slug }` }
			href={ `/post?slug=${ post.slug }&apiRoute=post` }
			prefetch
		>
			<a className="post-excerpt__link">
				<article id={ `post-${ post.id }` } className="post-excerpt">
					<span className="post-excerpt__meta">{ date }</span>
					<h3 className="post-excerpt__title">{ post.title }</h3>
				</article>
			</a>
		</Link>
	);
};

export default PostExcerpt;
