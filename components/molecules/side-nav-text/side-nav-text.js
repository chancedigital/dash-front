import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class SideNavText extends Component {
	navEl = document.createElement( 'div' );

	componentDidMount() {
		document.querySelector( '.layout__inner' ).appendChild( this.navEl );
	}

	componentWillUnmount() {
		document.querySelector( '.layout__inner' ).removeChild( this.navEl );
	}

	renderItems = items =>
		items.map( ( item, i ) => {
			const { onItemClick, activeItem } = this.props;
			const classNames = [
				'side-nav-text__item',
				`side-nav-text__item--${ i }`,
				activeItem === i ? 'side-nav-text__item--active' : '',
			];

			return (
				<li className={ classNames.join( ' ' ) } key={ i }>
					<span
						className="side-nav-text__label"
						onClick={ () => onItemClick( i, item ) }
					>
						{ item.label }
					</span>
					{ item.subItems &&
						item.subItems.length > 0 && (
							<ul className="side-nav-text__list side-nav-text__list--sub">
								{ this.renderItems( item.subItems ) }
							</ul>
						) }
				</li>
			);
		} );

	render() {
		const { items, className } = this.props;
		const nav = (
			<nav
				className={
					className
						? `side-nav-text side-nav-text--${ className }`
						: 'side-nav-text'
				}
			>
				{ items &&
					items.length > 0 && (
						<ul className="side-nav-text__list">
							{ this.renderItems( items ) }
						</ul>
					) }
			</nav>
		);
		return ReactDOM.createPortal( nav, this.navEl );
	}
}

export default SideNavText;
