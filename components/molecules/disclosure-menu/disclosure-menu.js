import React from 'react';
import Link from 'next/link';
import cx from 'classnames';
import disclosures from 'util/disclosures';
import PropTypes from 'prop-types';

const DisclosureMenu = ( { className, handleMenuClose } ) => (
	<span className={ cx( 'disclosure-menu', className ) }>
		{ disclosures.map( ( disc, i ) => {
			const sep = disclosures.length !== i + 1 ? ' | ' : '';
			const gap = i === 0 ? ' ' : '';
			return (
				<span className="disclosure-menu__item" key={ disc.id }>
					{ gap }
					<Link href={ disc.url }>
						<a
							className="disclosure-menu__link"
							target="_blank"
							onClick={ handleMenuClose }
						>
							{ disc.label }
						</a>
					</Link>
					{ sep }
				</span>
			);
		} ) }
	</span>
);

DisclosureMenu.propTypes = {
	className: PropTypes.string,
	handleMenuClose: PropTypes.func,
};

export default DisclosureMenu;
