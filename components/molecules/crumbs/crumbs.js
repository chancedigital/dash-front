import React from 'react';
import DotNav from 'molecules/dot-nav';
import { CSSTransition } from 'react-transition-group';
import getAnimationClassNames from 'util/get-animation-class-names';
import PropTypes from 'prop-types';

const Crumbs = ( { onDotClick, active, labels, pageName } ) => (
	<div className="crumbs">
		<DotNav
			sections={ labels }
			onDotClick={ onDotClick }
			active={ active }
			layout="horizontal"
		/>
		<div className="crumbs__labels">
			<span className="crumbs__page">{ pageName }</span>
			<span className="crumbs__sep">{ ' | ' }</span>
			{ labels && labels.length > 0 && labels.map( ( label, i ) => (
				<CSSTransition
					key={ i }
					in={ i === active }
					timeout={ 500 }
					appear={ true }
					classNames={ getAnimationClassNames( 'crumbs__active' ) }
					unmountOnExit
				>
					<span className="crumbs__active">{ label }</span>
				</CSSTransition>
			) ) }
		</div>
	</div>
);

Crumbs.propTypes = {
	onDotClick: PropTypes.func.isRequired,
	active: PropTypes.number,
	labels: PropTypes.array.isRequired,
	pageName: PropTypes.string.isRequired,
};

Crumbs.defaultProps = {
	active: 0,
};

export default Crumbs;
