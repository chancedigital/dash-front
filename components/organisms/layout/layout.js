import React, { Component } from 'react';
import classNames from 'classnames';
import Head from 'next/head';
import PropTypes from 'prop-types';
import 'what-input';
import BgVideo from 'molecules/bg-video';

class Layout extends Component {
	static propTypes = {
		bgImg: PropTypes.string,
		bgVideo: PropTypes.string,
		bgOverlay: PropTypes.oneOfType( [
			PropTypes.node,
			PropTypes.arrayOf( PropTypes.node ),
		] ),
		className: PropTypes.string,
		title: PropTypes.string,
	};

	static defaultProps = {
		title: 'Dash',
	};

	render() {
		const {
			children,
			bgImg,
			bgVideo,
			bgOverlay,
			className,
			title,
		} = this.props;
		const style = bgImg
			? {
					backgroundImage: `url( ${ bgImg } )`,
			  }
			: {};

		return (
			<div className={ classNames( 'layout', className ) } style={ style }>
				<Head>
					<title>{ title }</title>
					<link
						rel="shortcut icon"
						type="image/x-icon"
						href="/static/favicon.png"
					/>
				</Head>
				{ bgVideo && <BgVideo src={ bgVideo } /> }
				{ bgOverlay && bgOverlay }
				<div className="layout__inner">{ children }</div>
			</div>
		);
	}
}

export default Layout;
