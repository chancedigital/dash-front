import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class OutsideAlerter extends Component {
	static propTypes = {
		children: PropTypes.element.isRequired,
		onOutsideClick: PropTypes.func,
	};

	static defaultProps = {
		onOutsideClick: () => null
	};

	setWrapperRef = this.setWrapperRef;

	componentDidMount() {
		document.addEventListener( 'mousedown', this.handleClickOutside );
	}

	componentWillUnmount() {
		document.removeEventListener( 'mousedown', this.handleClickOutside );
	}

	/**
	 * Set the wrapper ref
	 */
	setWrapperRef = node => {
		this.wrapperRef = node;
	};

	/**
	 * Alert if clicked on outside of element
	 */
	handleClickOutside = e => {
		if ( this.wrapperRef && ! this.wrapperRef.contains( e.target ) ) {
			this.props.onOutsideClick();
		}
	};

	render() {
		return <div ref={ this.setWrapperRef }>{ this.props.children }</div>;
	}
}
