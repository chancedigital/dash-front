import React, { Component } from 'react';
import { CSSTransition } from 'react-transition-group';
import ReactResizeDetector from 'react-resize-detector';
import PropTypes from 'prop-types';
import getScrollDirection from 'util/get-scroll-direction';
import getPosition from 'util/get-element-position';
import { DeviceContext } from 'context/device';

class Scroller extends Component {
	/**
	 * Component state.
	 */
	state = {
		isScrolling: false,
		enteredProps: {},
		entered: {},
	};

	/**
	 * Container element ref.
	 */
	containerEl = React.createRef();

	/**
	 * Prop types.
	 */
	static propTypes = {
		active: PropTypes.number.isRequired,
		onActiveChange: PropTypes.func.isRequired,
		enableSwipe: PropTypes.oneOf( [ 'horizontal', 'vertical', false ] ),
		startAtBreakpoint: PropTypes.number,
	};

	/**
	 * Default props.
	 */
	static defaultProps = {
		enableSwipe: false,
		startAtBreakpoint: 0,
	};

	/**
	 * Initiate our timeout variable
	 * Used to make sure we aren't scrolling faster than the components can render.
	 */
	_timeout = null;

	/**
	 * Fire when the component initially mounts.
	 */
	componentDidMount() {
		// Listen for wheel & touch events
		window.addEventListener( 'wheel', this.handleScroll );
	}

	/**
	 * Fire just before the component unmounts.
	 * Remove event listeners and timeouts to prevent memory leaks.
	 */
	componentWillUnmount() {
		if ( this._timeout ) {
			clearTimeout( this._timeout );
		}
		window.removeEventListener( 'wheel', this.handleScroll );
	}

	/**
	 * Prevent re-render when certain state is changed.
	 * @param {object} nextProps Incoming props.
	 * @param {object} nextState Incoming state.
	 */
	shouldComponentUpdate( nextProps, nextState ) {
		if ( this.state.isScrolling !== nextState.isScrolling ) {
			return false;
		}
		if ( this.state.entered !== nextState.entered ) {
			return false;
		}
		return true;
	}

	/**
	 * Update the measured dimensions of the current slide.
	 * This helps us maintain position of the existing slide when it is
	 * positioned absolute or fixed.
	 */
	updateCurrentSlideProps = entered => {
		const coordinates = getPosition( entered );
		const enteredProps = {
			height: entered.clientHeight,
			width: entered.clientWidth,
			...coordinates,
		};
		this.setState(
			entered !== this.state.entered
				? { entered, enteredProps }
				: { enteredProps },
		);
	};

	/**
	 * Style the exisitng slide to ensure it doesn't interfere with the
	 * entering slide.
	 */
	styleLeavingSlide = leaving => {
		const { height, width, x, y } = this.state.enteredProps;
		const unit = 'px';
		Object.assign( leaving.style, {
			position: 'fixed',
			height: height + unit,
			width: width + unit,
			left: x + unit,
			top: y + unit,
		} );
	};

	/**
	 * Prevent rapid succession of slide changes when scrolling too fast.
	 */
	scrollTimeout = () => {
		if ( this._timeout ) {
			clearTimeout( this._timeout );
		}
		this._timeout = setTimeout( () => {
			this._timeout = null;
			this.setState( { isScrolling: false } );
		}, 250 );
	};

	/**
	 * Handle slide changes.
	 * Fires the onActiveChange function passed by the parent after the state is set.
	 */
	onSlideChange = newActive => {
		const { onActiveChange } = this.props;
		this.setState( { isScrolling: true }, () => onActiveChange( newActive ) );
	};

	/**
	 * Fires when the user scrolls.
	 */
	handleScroll = e => {
		const { children, active, device } = this.props;
		if (
			device === 'desktop' &&
			children &&
			children.length > 1 &&
			! e.target.closest( '.has-scroll' )
		) {
			const lastSlide = children.length - 1;
			this.scrollTimeout();
			if ( ! this.state.isScrolling ) {
				const scrollDir = getScrollDirection( e, 'vertical' );
				let newActive = active;

				if ( scrollDir && scrollDir === 'up' && active > 0 ) {
					// If scrolling up, to back
					newActive = active - 1;
				} else if ( scrollDir && scrollDir === 'down' && active < lastSlide ) {
					// If scrolling down, to forward
					newActive = active + 1;
				}

				if ( newActive !== active ) {
					this.onSlideChange( newActive );
				}
			}
		}
	};

	/**
	 * Render the component.
	 */
	render() {
		const { children, active, device } = this.props;
		const { entered } = this.state;
		const className = this.props.className
			? this.props.className.split( ' ' ).splice( 0, 1 )
			: 'scroller__transition';

		return device === 'desktop' ? (
			<div className="scroller" ref={ this.containerEl }>
				{ React.Children.map( children, ( child, i ) => {
					return (
						<CSSTransition
							in={ i === active }
							key={ i }
							timeout={ 1000 }
							appear={ true }
							classNames={ `${ className }-` }
							onEntered={ this.updateCurrentSlideProps }
							onExiting={ this.styleLeavingSlide }
							unmountOnExit
						>
							<div className="scroller__inner">
								<ReactResizeDetector
									handleWidth
									handleHeight
									onResize={ () => this.updateCurrentSlideProps( entered ) }
									refreshMode="debounce"
									refreshRate={ 10 }
								>
									{ React.cloneElement( child, {
										...child.props,
									} ) }
								</ReactResizeDetector>
							</div>
						</CSSTransition>
					);
				} ) }
			</div>
		) : (
			children
		);
	}
}

export default props => (
	<DeviceContext.Consumer>
		{ device => <Scroller { ...props } device={ device } /> }
	</DeviceContext.Consumer>
);
