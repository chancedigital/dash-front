import React from 'react';
import { CSSTransition } from 'react-transition-group';

export default React.forwardRef(
	( { index, activeProduct, onTransitionEnter, title, content, acf }, ref ) => (
		<CSSTransition
			in={ index === activeProduct }
			timeout={ 1000 }
			appear={ true }
			classNames="product-category__product-content-wrapper-"
			onEnter={ onTransitionEnter }
			unmountOnExit
		>
			<div className="product-category__product-content-wrapper" ref={ ref }>
				<h2
					className="product-category__title"
					dangerouslySetInnerHTML={ {
						__html: title.replace('™', '<span class="trademark">™</span>'),
					} }
				/>
				{ acf &&
					acf.description && (
						<p className="product-category__description">{ acf.description }</p>
					) }
				<div
					className="product-category__content"
					dangerouslySetInnerHTML={ {
						__html: content,
					} }
				/>
			</div>
		</CSSTransition>
	),
);
