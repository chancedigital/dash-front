import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import ProductCategoryNavItem from '../product-category-nav-item';

const ProductCategoryNav = ( {
	className,
	products,
	setActiveProduct,
	activeProduct,
} ) => (
	<nav className={ classNames( 'product-category__nav', className ) }>
		<ul className="product-category__nav-menu">
			{ products.length && products.map( ( product, i ) => {
				const itemClass = classNames( 'product-category__nav-item', {
					'product-category__nav-item--active': activeProduct === i,
				} );
				return (
					<ProductCategoryNavItem
						key={ product.id }
						itemClass={ itemClass }
						onButtonClick={ e => setActiveProduct( e, i ) }
						title={ product.title.rendered }
					/>
				);
			} ) }
		</ul>
	</nav>
);

ProductCategoryNav.propTypes = {
	className: PropTypes.string,
	products: PropTypes.array.isRequired,
	setActiveProduct: PropTypes.func.isRequired,
	activeProduct: PropTypes.number.isRequired,
};

export default ProductCategoryNav;
