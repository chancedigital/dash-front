import React from 'react';
import ReactResizeDetector from 'react-resize-detector';
import { CSSTransition } from 'react-transition-group';
import { DeviceContext } from 'context/device';

const ProductCategoryImage = ( {
	index,
	activeProduct,
	onImgResize,
	imgUrl,
	imgAlt,
} ) => (
	<DeviceContext.Consumer>
		{ device =>
			device !== 'mobile' ? (
				<ReactResizeDetector
					key={ index }
					handleHeight
					onResize={ onImgResize }
				>
					<CSSTransition
						in={ index === activeProduct }
						timeout={ 1000 }
						appear={ true }
						classNames="product-category__img-"
						unmountOnExit
					>
						<img
							className="product-category__img"
							src={ imgUrl }
							alt={ imgAlt }
						/>
					</CSSTransition>
				</ReactResizeDetector>
			) : (
				<CSSTransition
					in={ index === activeProduct }
					timeout={ 1000 }
					appear={ true }
					classNames="product-category__img-"
					unmountOnExit
				>
					<img
						className="product-category__img"
						src={ imgUrl }
						alt={ imgAlt }
					/>
				</CSSTransition>
			)
		}
	</DeviceContext.Consumer>
);

export default ProductCategoryImage;
