import React from 'react';

const ProductCategoryNavItem = ( { itemClass, onButtonClick, title } ) => (
	<li className={ itemClass }>
		<button className="product-category__nav-link" onClick={ onButtonClick }>
			<span
				dangerouslySetInnerHTML={ {
					__html: title
						.replace( /\s?\(([^\)]+)\)/g, '' )
						.replace( /Dash\s/g, '' ),
				} }
			/>
		</button>
	</li>
);

export default ProductCategoryNavItem;
