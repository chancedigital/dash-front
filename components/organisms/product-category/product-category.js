import React, { Component } from 'react';
import ReactResizeDetector from 'react-resize-detector';
import classNames from 'classnames';
import ProductCategoryImage from './components/product-category-image';
import ProductCategoryContent from './components/product-category-content';
import ProductCategoryNav from './components/product-category-nav';

class ProductCategory extends Component {
	state = {
		activeProduct: 0,
		LastDescriptionWrapperSize: {},
		NextDescriptionWrapperSize: {},
		descriptionWrapperHeight: 0,
		imgWrapperHeight: 0,
	};

	descriptionWrapper = React.createRef();
	imgWrapper = React.createRef();

	setActiveProduct = ( e, activeProduct ) => {
		e.preventDefault();
		this.setState( { activeProduct } );
	};

	onDescriptionResize = ( sectionWidth, sectionHeight ) => {
		const descriptionWrapperHeight = sectionHeight;
		this.setState( { descriptionWrapperHeight } );
	};

	onImgResize = ( sectionWidth, sectionHeight ) => {
		const imgWrapperHeight = sectionHeight;
		this.setState( { imgWrapperHeight } );
	};

	render() {
		const { category, products } = this.props;
		const { activeProduct } = this.state;

		if ( products && products.length > 0 ) {
			const { acf } = products[ activeProduct ];
			return (
				<article className="product-category">
					<span
						className="product-category__cat-name product-category__cat-name--small"
						dangerouslySetInnerHTML={ { __html: category.name } }
					/>
					{ products.length > 1 && (
						<ProductCategoryNav
							className="product-category__nav--small"
							products={ products }
							setActiveProduct={ this.setActiveProduct }
							activeProduct={ activeProduct }
						/>
					) }
					<div
						className="product-category__img-wrapper"
						ref={ this.imgWrapper }
					>
						{ products.map( ( product, i ) => {
							const img = product._embedded[ 'wp:featuredmedia' ][ 0 ];
							if ( img ) {
								return (
									<ProductCategoryImage
										key={ i }
										index={ i }
										activeProduct={ activeProduct }
										onImgResize={ this.onImgResize }
										imgUrl={ img.source_url }
										imgAlt={ img.alt_text }
									/>
								);
							}
						} ) }
					</div>
					<div className="product-category__content-wrapper">
						<ReactResizeDetector
							handleHeight
							onResize={ this.onDescriptionResize }
						>
							<span
								className="product-category__cat-name product-category__cat-name--medium"
								dangerouslySetInnerHTML={ { __html: category.name } }
							/>
							{ products.map( ( product, i ) => {
								const { title, content, acf } = product;
								return (
									<ProductCategoryContent
										ref={ this.descriptionWrapper }
										key={ i }
										index={ i }
										activeProduct={ activeProduct }
										onTransitionEnter={ () =>
											this.setState( {
												LastDescriptionWrapperSize: {},
											} )
										}
										title={ title.rendered }
										content={ content.rendered }
										acf={ acf }
									/>
								);
							} ) }{' '}
							{ products.length > 1 && (
								<ProductCategoryNav
									className="product-category__nav--medium"
									products={ products }
									setActiveProduct={ this.setActiveProduct }
									activeProduct={ activeProduct }
								/>
							) }
						</ReactResizeDetector>
					</div>
				</article>
			);
		}
	}
}

export default ProductCategory;
