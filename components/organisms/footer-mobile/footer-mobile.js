import React from 'react';
import Copyright from 'atoms/copyright';
import cx from 'classnames';
import PropTypes from 'prop-types';
import DisclosureMenu from 'molecules/disclosure-menu';

const FooterMobile = ({ className }) => {
	return (
		<div className={ cx( 'footer-mobile', className ) }>
			<div className="footer-mobile__inner">
				<Copyright className="footer-mobile__copyright" />
				<DisclosureMenu className="footer-mobile__disclosure-menu" />
			</div>
		</div>
	);
};

FooterMobile.propTypes = {
	className: PropTypes.string,
};

export default FooterMobile;
