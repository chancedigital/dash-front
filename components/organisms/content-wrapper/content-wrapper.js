import React, { Component } from 'react';

class ContentWrapper extends Component {
	constructor( props ) {
		super( props );
		this.wrapperRef = React.createRef();
	}

	render() {
		return <div ref={ this.wrapperRef } className="content-wrapper">{ this.props.children }</div>;
	}
}

export default ContentWrapper;
