import React, { Component } from 'react';
import { CSSTransition } from 'react-transition-group';
import NoSSR from 'react-no-ssr';
import ScrollBar from 'react-perfect-scrollbar';
import PostExcerpt from 'molecules/post-excerpt';
import SideNavText from 'molecules/side-nav-text';
import isLoadedArray from 'util/is-loaded-array';

/**
 * Post grid component.
 */
class PostGrid extends Component {
	/**
	 * Component state.
	 */
	state = {
		filteredPosts: this.props.posts ? this.props.posts : [],
		activeNavItem: 0,
	};

	/**
	 * Ref for the section wrapper and scroller DOM elements.
	 */
	sectionWrapper = React.createRef();
	sectionScroller = React.createRef();

	/**
	 * Handle the post filter functionality.
	 */
	handlePostFilter = ( activeNavItem, item ) => {
		const { label, id } = item;
		const posts = isLoadedArray( this.props.posts );
		if ( label === 'News' ) {
			const filteredPosts = posts;
			this.setState( { filteredPosts, activeNavItem } );
		} else if ( isNaN( label ) && typeof id !== 'undefined' ) {
			// Non-numeric items are category filters.
			const filteredPosts = posts.filter( post =>
				isLoadedArray( post.categories )
					.map( cat => cat.id )
					.includes( id ),
			);
			this.setState( { filteredPosts, activeNavItem } );
		} else if ( ! isNaN( label ) ) {
			// Year filters
			const filteredPosts = posts.filter(
				post => post.date.indexOf( label ) === 0,
			);
			this.setState( { filteredPosts, activeNavItem } );
		}
	};

	getFilterOptions = () => {
		const { navItems } = this.props;
		if ( navItems && navItems.length ) {
			return navItems.map( ( item, index ) => ( {
				id: index,
				value: index,
				label: item.label,
				item,
			} ) );
		}
	};

	handleSelectChange = e => {
		const selected = JSON.parse( e.target.value );
		this.handlePostFilter( selected.value, selected.item );
	};

	/**
	 * Render the component.
	 */
	render() {
		const { posts, navItems, isActive } = this.props;
		const { filteredPosts, activeNavItem } = this.state;
		const selectOptions = this.getFilterOptions();
		return (
			<div className="post-grid has-scroll" ref={ this.sectionWrapper }>
				<form className="post-grid__select-form">
					<label className="post-grid__select-label label-as-wrapper">
						<span className="label">Filter Posts</span>
						<select
							className="post-grid__select"
							onChange={ this.handleSelectChange }
						>
							{ selectOptions.map( item => (
								<option key={ item.id } value={ JSON.stringify( item ) }>
									{ item.label }
								</option>
							) ) }
						</select>
					</label>
				</form>
				<ScrollBar className="post-grid__items">
					{ posts.map( ( post, i ) => (
						<CSSTransition
							in={ filteredPosts.includes( post ) }
							key={ i }
							timeout={ 1000 }
							appear={ true }
							classNames="post-grid__post-"
							unmountOnExit
						>
							<div className="post-grid__post">
								<PostExcerpt post={ post } />
							</div>
						</CSSTransition>
					) ) }
				</ScrollBar>

				{ isLoadedArray( navItems ) && (
					<NoSSR>
						<CSSTransition
							in={ true }
							timeout={ 1000 }
							appear={ true }
							classNames="post-grid__side-nav-"
							unmountOnExit
						>
							<SideNavText
								className={ isActive ? 'active' : null }
								onItemClick={ this.handlePostFilter }
								items={ navItems }
								activeItem={ activeNavItem }
							/>
						</CSSTransition>
					</NoSSR>
				) }
			</div>
		);
	}
}

export default PostGrid;
