import React, { Component } from 'react';
import Swipeable from 'react-swipeable';
import { CSSTransition } from 'react-transition-group';
import ReactResizeDetector from 'react-resize-detector';
import getPosition from 'util/get-element-position';
import PropTypes from 'prop-types';

class Swiper extends Component {
	/**
	 * Component state.
	 */
	state = {
		isScrolling: false,
		enteredProps: {},
		entered: {},
	};

	/**
	 * Prop types.
	 */
	static propTypes = {
		active: PropTypes.number.isRequired,
		onActiveChange: PropTypes.func.isRequired,
		swipeMode: PropTypes.oneOf( [ 'horizontal', 'vertical' ] ),
		screenWidth: PropTypes.number,
	};

	/**
	 * Default props.
	 */
	static defaultProps = {
		swipeMode: 'vertical',
		screenWidth: 0,
	};

	/**
	 * Initiate our timeout variable
	 * Used to make sure we aren't scrolling faster than the components can render.
	 */

	/**
	 * Prevent re-render when certain state is changed.
	 * @param {object} nextProps Incoming props.
	 * @param {object} nextState Incoming state.
	 */
	shouldComponentUpdate( nextProps, nextState ) {
		if ( this.state.isScrolling !== nextState.isScrolling ) {
			return false;
		}
		if ( this.state.entered !== nextState.entered ) {
			return false;
		}
		return true;
	}

	/**
	 * Update the measured dimensions of the current slide.
	 * This helps us maintain position of the existing slide when it is
	 * positioned absolute or fixed.
	 */
	updateCurrentSlideProps = entered => {
		const coordinates = getPosition( entered );
		const enteredProps = {
			height: entered.clientHeight,
			width: entered.clientWidth,
			...coordinates,
		};
		this.setState(
			entered !== this.state.entered
				? { entered, enteredProps }
				: { enteredProps },
		);
	};

	/**
	 * Style the exisitng slide to ensure it doesn't interfere with the
	 * entering slide.
	 */
	styleLeavingSlide = leaving => {
		const { height, width, x, y } = this.state.enteredProps;
		const unit = 'px';
		Object.assign( leaving.style, {
			position: 'fixed',
			height: height + unit,
			width: width + unit,
			left: x + unit,
			top: y + unit,
		} );
	};

	/**
	 * Handle slide changes.
	 * Fires the onActiveChange function passed by the parent after the state is set.
	 */
	onSlideChange = newActive => {
		const { onActiveChange } = this.props;
		this.setState( { isScrolling: true }, () => onActiveChange( newActive ) );
	};

	handleSwipe = ( e, deltaX, deltaY, isFlick, velocity ) => {
		const { children, active, swipeMode, screenWidth } = this.props;
		if (
			children &&
			children.length > 1 &&
			! e.target.closest( '.has-scroll' )
		) {
			let swipeDir;
			if ( deltaY > 0 && deltaX === -0 ) {
				swipeDir = 'down';
			} else if ( deltaY <= 0 && deltaX === -0 ) {
				swipeDir = 'up';
			} else if ( deltaY === -0 && deltaX > 0 ) {
				swipeDir = 'left';
			} else if ( deltaY === -0 && deltaX <= 0 ) {
				swipeDir = 'right';
			} else if ( deltaY > 0 && deltaX > 0 ) {
				swipeDir = 'down left';
			} else if ( deltaY > 0 && deltaX <= 0 ) {
				swipeDir = 'down right';
			} else if ( deltaY <= 0 && deltaX > 0 ) {
				swipeDir = 'up left';
			} else if ( deltaY <= 0 && deltaX <= 0 ) {
				swipeDir = 'up right';
			} else {
				swipeDir = null;
			}
			const lastSlide = children.length - 1;
			if ( ! this.state.isScrolling ) {
				let newActive = active;
				if ( swipeMode === 'vertical' ) {
					if ( swipeDir && swipeDir.includes( 'up' ) && active > 0 ) {
						// If scrolling up, to back
						newActive = active - 1;
					} else if (
						swipeDir &&
						swipeDir.includes( 'down' ) &&
						active < lastSlide
					) {
						// If scrolling down, to forward
						newActive = active + 1;
					}
				} else if ( swipeMode === 'horizontal' ) {
					if ( swipeDir && swipeDir.includes( 'left' ) && active > 0 ) {
						// If scrolling left, to back
						newActive = active - 1;
					} else if (
						swipeDir &&
						swipeDir.includes( 'right' ) &&
						active < lastSlide
					) {
						// If scrolling right, to forward
						newActive = active + 1;
					}
				}

				if ( newActive !== active ) {
					this.onSlideChange( newActive );
				}
			}
		}
	};

	/**
	 * Render the component.
	 */
	render() {
		const { children, active } = this.props;
		const { entered } = this.state;
		const className = this.props.className
			? this.props.className.split( ' ' ).splice( 0, 1 )
			: 'swiper__transition';

		return children && children.length > 1 ? (
			<Swipeable
				onSwiped={ this.handleSwipe }
				preventDefaultTouchmoveEvent={ true }
			>
				<div className="swiper">
					{ React.Children.map( children, ( child, i ) => {
						return (
							<CSSTransition
								in={ i === active }
								key={ i }
								timeout={ 1000 }
								appear={ true }
								classNames={ `${ className }-` }
								onEntered={ this.updateCurrentSlideProps }
								onExiting={ this.styleLeavingSlide }
								unmountOnExit
							>
								<div className="swiper__inner">
									<ReactResizeDetector
										handleWidth
										handleHeight
										onResize={ () => this.updateCurrentSlideProps( entered ) }
										refreshMode="debounce"
										refreshRate={ 10 }
									>
										{ React.cloneElement( child, {
											...child.props,
										} ) }
									</ReactResizeDetector>
								</div>
							</CSSTransition>
						);
					} ) }
				</div>
			</Swipeable>
		) : (
			children
		);
	}
}

export default Swiper;
