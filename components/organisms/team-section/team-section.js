import React, { Component } from 'react';
import { CSSTransition } from 'react-transition-group';
import TeamMember from 'molecules/team-member';
import SideNavText from 'molecules/side-nav-text';
import getAnimationClassNames from 'util/get-animation-class-names';
import NoSSR from 'react-no-ssr';

class TeamSection extends Component {
	state = {
		activeTeamMember: 0,
	};

	setActiveTeamMember = activeTeamMember => {
		this.setState( { activeTeamMember } );
	};

	handleSelectChange = e => {
		this.setActiveTeamMember( parseInt( e.target.value ) );
	};

	getNameOptions = () => {
		const { teamPosts } = this.props;
		const { activeTeamMember } = this.state;
		if ( teamPosts && teamPosts.length ) {
			return teamPosts.map( ( { id, title }, index ) => ( {
				id,
				value: index,
				label: title.rendered,
				//props: { selected: activeTeamMember === index },
			} ) );
		}
	};

	render() {
		const { teamPosts, isActive } = this.props;
		const { activeTeamMember } = this.state;
		const selectOptions = this.getNameOptions();
		const navItems = teamPosts.map( post => ( {
			label: post.title.rendered,
		} ) );

		if ( teamPosts && teamPosts.length ) {
			return (
				<div className="team-section">
					<form className="team-section__select-form">
						<label className="team-section__select-label label-as-wrapper">
							<span className="label">Select Team Member</span>
							<select
								className="team-section__select"
								onChange={ this.handleSelectChange }
							>
								{ selectOptions.map( ( { id, value, label } ) => (
									<option key={ id } value={ value }>
										{ label }
									</option>
								) ) }
							</select>
						</label>
					</form>

					{ teamPosts.map( ( teamPost, i ) => {
						const teamProps = {
							img: teamPost._embedded[ 'wp:featuredmedia' ][ 0 ],
							bio: teamPost.content.rendered,
							jobTitle: teamPost.acf.job_title,
							name: teamPost.title.rendered,
							get hasImg() {
								return !! ( this.img && this.img.source_url );
							},
						};
						return (
							<CSSTransition
								key={ `transition-${ teamPost.id }` }
								in={ i === activeTeamMember }
								timeout={ 1000 }
								appear={ true }
								classNames={ getAnimationClassNames( 'team-section__member' ) }
								unmountOnExit
							>
								<TeamMember key={ `member-${ teamPost.id }` } { ...teamProps } />
							</CSSTransition>
						);
					} ) }
					<NoSSR>
						<CSSTransition
							in={ true }
							timeout={ 1000 }
							appear={ true }
							classNames={ getAnimationClassNames( 'team-section__side-nav' ) }
							unmountOnExit
						>
							<SideNavText
								activeItem={ this.state.activeTeamMember }
								className={ isActive ? 'active' : null }
								onItemClick={ this.setActiveTeamMember }
								items={ navItems }
							/>
						</CSSTransition>
					</NoSSR>
				</div>
			);
		}
	}
}

export default TeamSection;
