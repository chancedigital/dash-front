import React, { Component } from 'react';
import Router from 'next/router';
import PropTypes from 'prop-types';
import cx from 'classnames';
import DisclosureMenu from 'molecules/disclosure-menu';
import OutsideAlerter from 'organisms/outside-alerter';

class DisclosuresNav extends Component {
	state = {
		menuIsOpen: false,
		screenWidth: 0,
		loaded: false,
		closed: false,
	};

	handleMenuClose = () => {
		this.setState( state => {
			if ( state.menuIsOpen ) return { menuIsOpen: false, closed: true };
		} );
	};

	handleMenuChange = () => {
		this.setState( state => {
			return {
				menuIsOpen: ! state.menuIsOpen,
			};
		} );
	};

	handleClickOutside( event ) {
		if ( this.wrapperRef && ! this.wrapperRef.contains( event.target ) ) {
			alert( 'You clicked outside of me!' );
		}
	}

	componentDidMount() {
		this.setState( state => {
			if ( ! state.loaded ) return { loaded: true };
		} );
	}

	closeMenuOnRouteChange = () =>
		Router.events.on( 'routeChangeComplete', () => this.handleMenuClose );

	render() {
		const { menuIsOpen, loaded, closed } = this.state;
		return (
			<OutsideAlerter onOutsideClick={ this.handleMenuClose }>
				<nav
					className={ cx( 'disclosures-nav', {
						'disclosures-nav--active': menuIsOpen,
						'disclosures-nav--loaded': loaded,
						'disclosures-nav--closed': closed,
					} ) }
				>
					<button
						className="disclosures-nav__button"
						onClick={ this.handleMenuChange }
					>
						Disclosures
					</button>
					<div className="disclosures-nav__menu-wrapper">
						<button
							className="disclosures-nav__close"
							onClick={ this.handleMenuClose }
						>
							<span className="screen-reader-text">Close Menu</span>
						</button>
						<span className="disclosures-nav__text-wrapper">
							<span className="disclosures-nav__legal">
								&copy; { new Date().getFullYear() } Dash Financial Technologies.
								All Rights Reserved.{' '}
							</span>
							<DisclosureMenu
								handleMenuClose={ this.handleMenuClose }
								className="disclosures-nav__menu"
							/>
						</span>
					</div>
				</nav>
			</OutsideAlerter>
		);
	}
}

export default DisclosuresNav;
