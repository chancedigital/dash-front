import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import FooterMobile from 'organisms/footer-mobile';
import { DeviceContext } from 'context/device';

class FooterInner extends PureComponent {
	static propTypes = {
		leftComponent: PropTypes.node,
		rightComponent: PropTypes.node,
	};

	render() {
		const { leftComponent, rightComponent } = this.props;
		return (
			<CSSTransition
				key={ 'footer-inner' }
				in={ true }
				timeout={ 500 }
				appear={ true }
				classNames="footer-"
				unmountOnExit
			>
				<footer className="footer">
					<div className="footer-inner">
						{ leftComponent && (
							<div className="footer-inner__left">{ leftComponent }</div>
						) }
						{ rightComponent && (
							<div className="footer-inner__right">{ rightComponent }</div>
						) }
					</div>
					<DeviceContext.Consumer>
						{ device => device === 'mobile' && <FooterMobile /> }
					</DeviceContext.Consumer>
				</footer>
			</CSSTransition>
		);
	}
}

export default FooterInner;
