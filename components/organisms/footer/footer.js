import React, { Component } from 'react';
import Link from 'next/link';
import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types';
import DisclosureMenu from 'molecules/disclosure-menu';
import FooterMobile from 'organisms/footer-mobile';
import { DeviceContext } from 'context/device';

class Footer extends Component {
	static defaultProps = {
		anchorLeft: {
			label: 'Client Login',
			href: 'https://www.dashfinancial.com/dash360/',
			target: '_blank',
		},
		anchorRight: {
			label: 'Contact',
			href: '/contact',
			target: '_self',
		},
		anchorCenter: true,
	};

	render() {
		const {
			anchorLeft,
			anchorRight,
			anchorCenter,
			menuIsVisible,
			handleMenuChange,
		} = this.props;
		return (
			<CSSTransition
				key={ 'footer' }
				in={ true }
				timeout={ 500 }
				appear={ true }
				classNames="footer-"
				unmountOnExit
			>
				<footer className="footer">
					{ anchorLeft && (
						<div className="footer__anchor footer__anchor--left">
							<Link href={ anchorLeft.href } prefetch>
								<a
									onClick={ menuIsVisible ? handleMenuChange : null }
									className="footer__link footer__link--anchor"
									target={ anchorLeft.target === '_blank' ? '_blank' : '' }
									rel={
										anchorLeft.target === '_blank' ? 'noopener noreferrer' : ''
									}
								>
									{ anchorLeft.label }
								</a>
							</Link>
						</div>
					) }
					{ anchorCenter && (
						<div className="footer__anchor footer__anchor--center">
							<span className="footer__info">
								&copy; { new Date().getFullYear() } Dash Financial Technologies.
								All Rights Reserved.
								<DisclosureMenu className="footer__disclosure-menu" />
							</span>
						</div>
					) }
					{ anchorRight && (
						<div className="footer__anchor footer__anchor--right">
							<Link href={ anchorRight.href } prefetch>
								<a
									onClick={ menuIsVisible ? handleMenuChange : null }
									className="footer__link footer__link--anchor"
									target={ anchorRight.target === '_blank' ? '_blank' : '' }
									rel={
										anchorRight.target === '_blank' ? 'noopener noreferrer' : ''
									}
								>
									{ anchorRight.label }
								</a>
							</Link>
						</div>
					) }
					<DeviceContext.Consumer>
						{ device => device === 'mobile' && <FooterMobile /> }
					</DeviceContext.Consumer>
				</footer>
			</CSSTransition>
		);
	}
}

export default Footer;
