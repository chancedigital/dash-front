import React, { Component } from 'react';
import Router from 'next/router';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Footer from 'organisms/footer';
import BgVideo from 'molecules/bg-video';
import Menu from 'molecules/menu';
import { DeviceContext } from 'context/device';

class MainNav extends Component {
	static propTypes = {
		menu: PropTypes.object.isRequired,
		isVisible: PropTypes.bool,
		handleMenuChange: PropTypes.func,
	};

	closeMenuOnRouteChange = () =>
		Router.events.on( 'routeChangeComplete', () =>
			this.props.handleMenuClose(),
		);

	render() {
		const { menu, isVisible } = this.props;
		const navClass = classNames( {
			'main-nav': true,
			'main-nav--active': isVisible,
		} );
		return (
			<div className={ navClass }>
				{ isVisible && (
					<BgVideo src="https://res.cloudinary.com/chancedigital/video/upload/v1537371044/dash/video_menu.mp4" />
				) }
				<Menu
					handleMenuChange={ this.closeMenuOnRouteChange }
					className="main-nav__menu"
					menu={ menu }
					isOrderedList={ true }
				/>
				<DeviceContext.Consumer>
					{ device =>
						device !== 'mobile' && (
							<Footer
								menuIsVisible={ isVisible }
								handleMenuChange={ this.closeMenuOnRouteChange }
								anchorCenter={ false }
							/>
						)
					}
				</DeviceContext.Consumer>
			</div>
		);
	}
}

export default MainNav;
