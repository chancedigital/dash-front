import React, { Component } from 'react';
import { CSSTransition } from 'react-transition-group';
import NoSSR from 'react-no-ssr';
import SideNavText from 'molecules/side-nav-text';
import getAnimationClassNames from 'util/get-animation-class-names';
import breakpoints from 'util/breakpoints';

class DeepDive extends Component {
	state = {
		activeSection: 0,
	};

	setActiveSection = activeSection => {
		this.setState( { activeSection } );
	};

	render() {
		const { sections, isActive, screenWidth } = this.props;
		const { activeSection } = this.state;
		const navItems = sections.map( section => ( {
			label: section.title.rendered,
		} ) );

		if ( sections && sections.length > 0 ) {
			return (
				<div className="deep-dive">
					{ screenWidth >= breakpoints.medium
						? sections.map( ( section, i ) => {
								return (
									<CSSTransition
										key={ i }
										in={ i === activeSection }
										timeout={ 1000 }
										appear={ true }
										classNames={ getAnimationClassNames(
											'deep-dive__section',
										) }
										unmountOnExit
									>
										<div className="deep-dive__section">
											<h2 className="deep-dive__heading">
												{ section.title.rendered }
											</h2>
											<p className="deep-dive__content">
												{ section.content.rendered }
											</p>
										</div>
									</CSSTransition>
								);
						  } )
						: sections.map( ( section, i ) => {
								return (
									<div className="deep-dive__section" key={ i }>
										<h2 className="deep-dive__heading">
											{ section.title.rendered }
										</h2>
										<p className="deep-dive__content">
											{ section.content.rendered }
										</p>
									</div>
								);
						  } ) }
					{ screenWidth >= breakpoints.medium && (
						<NoSSR>
							<CSSTransition
								in={ true }
								timeout={ 1000 }
								appear={ true }
								classNames={ getAnimationClassNames( 'deep-dive__side-nav' ) }
								unmountOnExit
							>
								<SideNavText
									className={ isActive ? 'active' : null }
									onItemClick={ this.setActiveSection }
									items={ navItems }
									activeItem={ this.state.activeSection }
								/>
							</CSSTransition>
						</NoSSR>
					) }
				</div>
			);
		}
	}
}

export default DeepDive;
