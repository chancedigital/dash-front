import React, { PureComponent } from 'react';
import Head from 'next/head';
import Router from 'next/router';
import Masthead from 'molecules/masthead';
import MainNav from 'organisms/main-nav';
import Fonts from 'util/fonts.js';
import stylesheet from 'styles/style.scss';

class Header extends PureComponent {
	async componentDidMount() {
		Fonts();
	}

	handleMenuClose = () =>
		Router.events.on( 'routeChangeComplete', () =>
			this.props.handleMenuClose(),
		);

	render() {
		const { menu, menuIsOpen, handleMenuChange, device } = this.props;
		return (
			<div>
				<Head>
					<style dangerouslySetInnerHTML={ { __html: stylesheet } } />
					<meta name="viewport" content="width=device-width, initial-scale=1" />
					<meta charSet="utf-8" />
					<title>Dash</title>
				</Head>
				<Masthead
					handleMenuChange={ handleMenuChange }
					handleMenuClose={ this.handleMenuClose }
					menuIsOpen={ menuIsOpen }
				/>
				<MainNav
					handleMenuChange={ handleMenuChange }
					handleMenuClose={ this.handleMenuClose }
					isVisible={ menuIsOpen }
					menu={ menu }
				/>
			</div>
		);
	}
}

export default Header;
