import React from 'react';
import PropTypes from 'prop-types';

const FullScreenFrame = ( { children, onClick, onWheel } ) => (
	<div className="full-screen-frame" onClick={ onClick } onWheel={ onWheel }>
		<div className="full-screen-frame__inner">{ children }</div>
	</div>
);
FullScreenFrame.propTypes = {
	onClick: PropTypes.func,
	onWheel: PropTypes.func,
};
export default FullScreenFrame;
