import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NextLink from 'next/link';

export default class Link extends Component {
	static propTypes = {
		href: PropTypes.string.isRequired,
		active: PropTypes.bool,
	};

	static defaultProps = {
		active: false,
	};

	render() {
		const { children, href, active } = this.props;
		return (
			<NextLink href={ href } prefetch>
				<a active={ active }>
					{ children }
				</a>
			</NextLink>
		);
	}
}
