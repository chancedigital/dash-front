import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

const Copyright = ( { className } ) => (
	<span className={ cx( 'copyright', className ) }>
		&copy; { new Date().getFullYear() } Dash Financial Technologies. All Rights
		Reserved.
	</span>
);

Copyright.propTypes = {
	className: PropTypes.string,
};


export default Copyright;
