import React from 'react';
import { Field } from 'formik';

const Input = ( { type, name, label, erorrs, touched, showRequiredStar } ) => (
	<label className="label-as-wrapper input">
		<span className="screen-reader-text">{ label }:</span>
		<Field placeholder={ label } name={ name } type={ type } className="input__field" />
		{ erorrs && touched ? <div className="input__errors">{ erorrs }</div> : null }
		{ showRequiredStar && <span className="input__required">*</span> }
	</label>
);

export default Input;
