import React, { PureComponent } from 'react';
import classNames from 'classnames';

class Hamburger extends PureComponent {
	render() {
		const { menuIsOpen, handleMenuChange } = this.props;
		const buttonClass = classNames( {
			hamburger: true,
			'hamburger--active': menuIsOpen,
		} );
		return (
			<button
				title="menu"
				className={ buttonClass }
				onClick={ handleMenuChange }
			>
				<span className="hamburger__line" />
				<span className="hamburger__line" />
			</button>
		);
	}
}

export default Hamburger;
