import React from 'react';
import { Field } from 'formik';
import PropTypes from 'prop-types';

const Select = ( { name, label, options, hideLabel, ...rest } ) => (
	<label className="label-as-wrapper">
		{ hideLabel ? (
			<span className="screen-reader-text">{ label }:</span>
		) : (
			<span className="label">{ label }:</span>
		) }
		<Field component="select" name={ name } { ...rest }>
			{ options.map( ( { value, label, props, id } ) => (
				<option key={ id } value={ value } { ...props }>
					{ label }
				</option>
			) ) }
		</Field>
	</label>
);

Select.defaultProps = {
	hideLabel: true,
};

Select.propTypes = {
	hideLabel: PropTypes.bool,
	name: PropTypes.string.isRequired,
	label: PropTypes.string.isRequired,
	options: PropTypes.arrayOf(
		PropTypes.shape( {
			value: PropTypes.string.isRequired,
			label: PropTypes.string.isRequired,
			id: PropTypes.number.isRequired,
			props: PropTypes.object,
		} ),
	),
};

export default Select;
