import React, { PureComponent } from 'react';

// Dependencies
import PropTypes from 'prop-types';
import striptags from 'striptags';
import smart from 'util/smart-content';
import { AllHtmlEntities } from 'html-entities';
import { CSSTransition } from 'react-transition-group';
import classNames from 'classnames';

// Non-regex workaround
// @todo Maybe...debug babel error with regex.
const getList = str => {
	if ( str ) {
		const startTag = '[list]';
		const endTag = '[/list]';
		const startTagPosition = str.indexOf( startTag );
		const endTagPosition = str.indexOf( endTag );
		if ( startTagPosition === -1 || endTagPosition === -1 ) {
			return false;
		}
		const listStartPosition = startTagPosition + startTag.length;
		return str.substring( listStartPosition, endTagPosition );
	} else {
		return str;
	}
};

class DynamicText extends PureComponent {
	state = {
		currentHighlight: 0,
		items: [],
	};

	_interval = null;

	componentDidMount() {
		const items = this.getHighlightsArray();
		if ( items && items.length > 0 ) {
			this.setState( { items } );
			if ( ! this._interval ) {
				this._interval = setInterval( this.doThatThang, 1500 );
			}
		}
	}

	componentDidUpdate() {
		const { items, currentHighlight } = this.state;
		if ( items && items.length > 0 ) {
			if ( ! this._interval ) {
				this._interval = setInterval( this.doThatThang, 1500 );
			} else if ( currentHighlight === items.length - 1 ) {
				clearInterval( this._interval );
				this._interval = null;
				this._interval = setInterval( this.doThatThang, 4500 );
			} else if ( currentHighlight === 0 ) {
				clearInterval( this._interval );
				this._interval = null;
				this._interval = setInterval( this.doThatThang, 1500 );
			}
		} else if ( this._interval ) {
			clearInterval( this._interval );
			this._interval = null;
			this.setState( { currentHighlight: 0 } );
		}
	}

	componentWillUnmount() {
		if ( this._interval ) {
			clearInterval( this._interval );
		}
	}

	getHighlights = () => getList( this.props.content );

	getHighlightsArray = () => {
		const list = this.getHighlights();
		return list && list.length > 0
			? list.split( ',' ).map( word => word.trim() )
			: false;
	};

	// This function fires every 4 seconds, per componentDidMount
	doThatThang = () => {
		const items = this.getHighlightsArray();
		if ( items && items.length > 0 ) {
			this.setState( state => {
				const currentHighlight =
					state.currentHighlight < items.length - 1
						? state.currentHighlight + 1
						: 0;
				return { currentHighlight };
			} );
		}
	};

	render() {
		const entities = new AllHtmlEntities();
		const content = entities.decode( striptags( this.props.content ) );
		const items = this.getHighlightsArray();
		const baseClass = 'dynamic-text';
		const className = classNames( {
			'dynamic-text': true,
			[ this.props.className ]: !! this.props.className,
		} );
		if ( items && items.length > 0 ) {
			const itemsStr = this.getHighlights(); // e.g., 'beggining, middle, end'
			const parts = content.split( /\[\/?list\]/g ).filter( w => w ); // e.g., [ 'This is the ', 'beggining, middle, end', ' of the story' ]
			const highlightPosition = parts.indexOf( itemsStr ); // Position in the parts array of the shortcode content.
			return (
				<span className={ className }>
					{ parts.map( ( part, i ) => {
						if ( highlightPosition === i ) {
							return items.map( ( item, j, arr ) => {
								const isLast = this.state.currentHighlight === arr.length - 1;
								return (
									<CSSTransition
										in={ j === this.state.currentHighlight }
										key={ i + '-' + j }
										timeout={ 400 }
										classNames={ `${ baseClass }__highlight-` }
										unmountOnExit
									>
										<span
											key={ i }
											className={ classNames( `${ baseClass }__highlight`, {
												[ `${ baseClass }__highlight--last` ]: isLast,
											} ) }
										>
											{ item }
											{ isLast && '.' }
										</span>
									</CSSTransition>
								);
							} );
						} else {
							return part;
						}
					} ) }{' '}
				</span>
			);
		} else {
			return <span className={ className }>{ content }</span>;
		}
	}
}

export default DynamicText;

DynamicText.propTypes = {
	content: PropTypes.string.isRequired,
};
