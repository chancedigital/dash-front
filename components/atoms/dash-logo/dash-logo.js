import React from 'react';

const DashLogo = () => (
	<span className="dash-logo">
		<img src="/static/images/logo_dash.svg" alt="Dash Logo" />
	</span>
);

export default DashLogo;
