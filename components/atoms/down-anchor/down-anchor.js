import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const DownAnchor = ( { onClick, className, direction } ) => (
	<button
		className={ classNames( 'down-anchor', className ) }
		onClick={ onClick }
	>
		<span
			className={ classNames(
				'down-anchor__icon',
				`down-anchor__icon--${ direction }`,
			) }
			aria-hidden
		/>
		<span className="screen-reader-text">{ `Go to the ${
			direction === 'down' ? 'next' : 'previous'
		} section` }</span>
	</button>
);

DownAnchor.propTypes = {
	onClick: PropTypes.func.isRequired,
	className: PropTypes.string,
	direction: PropTypes.oneOf( [ 'up', 'down' ] ),
};

DownAnchor.defaultProps = {
	direction: 'down',
};

export default DownAnchor;
