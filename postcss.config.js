const path = require( 'path' );

module.exports = {
	plugins: {
		'postcss-svg': {
			svgo: process.env.NODE_ENV === 'production',
			dirs: [
				path.resolve( process.cwd(), 'static/images' ),
				path.resolve( process.cwd(), 'static/icons' ),
			],
		},
		'postcss-preset-env': { stage: 1 },
	},
};
