const express = require( 'express' );
const bodyParser = require( 'body-parser' );
const next = require( 'next' );
const Router = require( './controllers/routes' ).Router;
const dev = process.env.NODE_ENV !== 'production';
const app = next( { dev } );
const handle = app.getRequestHandler();

app
	.prepare()
	.then( () => {
		const server = express();
		server.use( bodyParser.json() );

		// Routing
		Router.forEachPattern( ( page, pattern, defaultParams ) =>
			server.get( pattern, ( req, res ) =>
				app.render(
					req,
					res,
					`/${ page }`,
					Object.assign( {}, defaultParams, req.query, req.params ),
				),
			),
		);

		server.get( '*', ( req, res ) => {
			return handle( req, res );
		} );

		// API endpoints
		server.use( '/api/v1', require( './controllers/contact' ) );

		server.listen( 3000, err => {
			if ( err ) throw err;
			console.log( '> Ready on http://localhost:3000' ); // eslint-disable-line
		} );
	} )
	.catch( ex => {
		console.error( ex.stack ); // eslint-disable-line
		process.exit( 1 );
	} );
