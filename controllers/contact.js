const express = require( 'express' );
const sendViaMailgun = require( '../services/mailgun' );
const router = express.Router();

router.post( '/contact', ( req, res, next ) => {
	sendViaMailgun( req )
		.then( response => res.status( 200 ).json( response ) )
		.catch( err => next( err || {} ) );
} );

module.exports = router;
