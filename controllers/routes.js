const UrlPrettifier = require( 'next-url-prettifier' ).default;

const routes = [
	{
		page: 'index',
		prettyUrl: '/',
		priority: '1.0',
	},
	{
		page: 'platform',
		prettyUrl: '/platform',
		prettyUrlPatterns: [
			{
				pattern: '/platform',
				defaultParams: {
					apiRoute: 'page',
					slug: 'platform',
				},
			},
		],
		priority: '.09',
	},
	{
		page: 'company',
		prettyUrl: '/company',
		prettyUrlPatterns: [
			{
				pattern: '/company',
				defaultParams: {
					apiRoute: 'page',
					slug: 'company',
				},
			},
		],
		priority: '.08',
	},
	{
		page: 'media',
		prettyUrl: '/media',
		prettyUrlPatterns: [
			{
				pattern: '/media',
				defaultParams: {
					activeSection: 0,
					apiRoute: 'page',
					slug: 'media',
				},
			},
		],
		priority: '.07',
	},
	{
		page: 'media',
		prettyUrl: '/media/posts',
		prettyUrlPatterns: [
			{
				pattern: '/media/posts',
				defaultParams: {
					activeSection: 1,
					apiRoute: 'page',
					slug: 'media',
				},
			},
		],
		priority: '.07',
	},
	{
		page: 'contact',
		prettyUrl: '/contact',
		prettyUrlPatterns: [
			{
				pattern: '/contact',
				defaultParams: {
					apiRoute: 'page',
					slug: 'contact',
				},
			},
		],
		priority: '.07',
	},
	{
		page: 'request-demo',
		prettyUrl: '/request-demo',
		prettyUrlPatterns: [
			{
				pattern: '/request-demo',
				defaultParams: {
					apiRoute: 'page',
					slug: 'request-demo',
				},
			},
		],
		priority: '.06',
	},
	{
		page: 'post',
		prettyUrl: ( { slug = '' } ) => `/media/${ slug }`,
		prettyUrlPatterns: [
			{
				pattern: '/media/:slug',
				defaultParams: {
					apiRoute: 'post',
				},
			},
		],
		priority: '.06',
	},
	{
		page: 'team',
		prettyUrl: ( { slug = '' } ) => `/team/${ slug }`,
		prettyUrlPatterns: [
			{
				pattern: '/team/:slug',
				defaultParams: {
					apiRoute: 'team',
				},
			},
		],
		priority: '.05',
	},
	{
		page: 'page',
		prettyUrl: ( { slug = '' } ) => `/${ slug }`,
		prettyUrlPatterns: [
			{
				pattern: '/:slug',
				defaultParams: {
					apiRoute: 'page',
				},
			},
			{
				pattern: '/page/:slug',
				defaultParams: {
					apiRoute: 'page',
				},
			},
		],
		priority: '.04',
	},
	{
		page: 'preview',
		prettyUrl: ( { id = '', wpnonce = '' } ) =>
			`/_preview/${ id }/${ wpnonce }`,
		prettyUrlPatterns: [ { pattern: '/_preview/:id/:wpnonce' } ],
		priority: '.01',
	},
];

const urlPrettifier = new UrlPrettifier( routes );
exports.default = routes;
exports.Router = urlPrettifier;
